#!/usr/bin/env python

import os
import sys
import subprocess
import pandas as pd
import yaml

from string import Template
#from snakemake_files.scripts.utils import getTargetInfo

def getRuns(config):
    ret = {}
    #LEN: Weird, but using pandas to handle the comments in the file
    #KEY: need skipinitialspace to make it fault tolerant to spaces!
    metadata = pd.read_csv(config['metasheet'], index_col=0, sep=',', comment='#', skipinitialspace=True)
    f = metadata.to_csv().split() #make it resemble an actual file with lines
    #SKIP the hdr
    for l in f[1:]:
        tmp = l.strip().split(",")
        #print(tmp)
        ret[tmp[0]] = tmp[1:]
        #print(ret)
        config['runs'] = ret
    return config

# def addCondaPaths_Config(config):
#     """ADDS the python2 paths to config"""
#     conda_root = subprocess.check_output('conda info --root',shell=True).decode('utf-8').strip()
#     config['conda_root'] = conda_root
#     config['wes_root'] = "%s/envs/wes" % conda_root

def addCondaPaths_Config(config):
    """ADDS the python2 paths to config"""
    conda_root = subprocess.check_output('conda info --root',shell=True).decode('utf-8').strip()
    conda_path = os.path.join(conda_root, 'pkgs')
    current_path = os.getcwd()
    config['conda_root'] = conda_root
    config['rnaseq_root'] = "%s/envs/rna" % conda_root
    config['optitype_root'] = "%s/envs/optitype_env" % conda_root
    config['stat_root'] = "%s/envs/stat_perl_r" % conda_root
    config['centrifuge_root']= "/%s/envs/centrifuge_env" % conda_root
    config['rseqc_root']= "/%s/envs/rseqc_env" % conda_root
    config['gatk4_root']= "/%s/envs/gatk4_env" % conda_root
    config['msisensor_root']= "/%s/envs/msisensor_env" % conda_root
    config['vep_root']= "/%s/envs/vep_env" % conda_root
    config['varscan_root']= "/%s/envs/varscan_env" % conda_root
    config['lisa_root']= "/%s/envs/lisa_env" % conda_root

    
def addExecPaths(config):
    conda_root = subprocess.check_output('conda info --root',shell=True).decode('utf-8').strip()
    conda_path = os.path.join(conda_root, 'pkgs')
    current_path = os.getcwd()
    #NEED the following when invoking python2 (to set proper PYTHONPATH)
    if not "pvacseq_path" in config or not config["pvacseq_path"]:
        config["pvacseq_path"] = os.path.join(conda_root, 'envs', 'pvacseq', 'bin')
    if not "trust4_path" in config or not config["trust4_path"]:
        config["trust4_path"] = os.path.join(current_path,'TRUST4')
    return config

def loadRef(config):
    f = open(config['ref'])
    ref_info = yaml.safe_load(f)
    f.close()
    #print(ref_info[config['assembly']])
    for (k,v) in ref_info[config['assembly']].items():
    #NO CLOBBERING what is user-defined!
        if k not in config:
            config[k] = v

def load_config(config_file):
    #load the main config file including parameters which are not change a lot
    with open(config_file, 'r') as stream:
        try:
            return yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            print(exc)

def load_execution(execution_file):
    #load the main config file including parameters which are not change a lot
    with open(execution_file, 'r') as stream:
        try:
            return yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            print(exc)

#---------  CONFIG set up  ---------------
config = load_config('config.yaml')
execution = load_execution('execution.yaml')
addCondaPaths_Config(config)
#config = getRuns(config)
loadRef(config)
addExecPaths(config)




#------------------------------------------------------------------------------
# TARGETS
#------------------------------------------------------------------------------
def all_targets(wildcards):
    ls = []
    #------------------------------------------------------------------------
    ##Level1
    #------------------------------------------------------------------------
    if execution["star"]:
        ls.extend(align_targets(wildcards))
        ls.extend(star_report_tagets(wildcards))
    if execution["salmon"]:
        ls.extend(salmon_targets(wildcards))
    if execution["rseqc"]:
        ls.extend(rseqc_targets(wildcards))
    #-----------------------------------------------------------------------
    ##Level2
    #----------------------------------------------------------------------
    if execution["batch_removal"]:
        ls.extend(combat_targets(wildcards))
    if execution["deseq2"]:
        ls.extend(deseq2_targets(wildcards))
    if execution["gsea"]:
        ls.extend(gsea_targets(wildcards))
    #--------------------------------------------------------------------    
    ##Level3
    #--------------------------------------------------------------------
    if execution["fusion"]:
        ls.extend(fusion_targets(wildcards))
    if execution["microbiota"]:
        ls.extend(microbe_targets(wildcards))
    if execution["trust4"]:
        ls.extend(trust4_targets(wildcards))
    if execution["optitype"]:
        ls.extend(optitype_targets(wildcards))
    #---------------------------------------------------------------------    
    ##Level4
    #---------------------------------------------------------------------
    if execution["tide"]:
        ls.extend(tide_targets(wildcards))
    if execution["immunedeconv"]:
        ls.extend(immunedeconv_targets(wildcards))
    if execution["wgcna"]:
        ls.extend(wgcna_targets(wildcards))
    if execution["ssgsea"]:
        ls.extend(ssgsea_targets(wildcards))
    if execution["msi_est"]:
        ls.extend(msi_targets(wildcards))
    #-------------------------------------------------------------------
    ##Level5
    #-------------------------------------------------------------------
    if execution["varscan"]:   
        ls.extend(varscan_targets(wildcards))
    if execution["vep"]: 
        ls.extend(vep_targets(wildcards))
    if execution["pvacseq"]:
        ls.extend(neoantigen_targets(wildcards))
    if execution["msisensor2"]:
        ls.extend(msisensor_targets(wildcards))
    if execution["lisa"]:
        ls.extend(lisa_targets(wildcards))
    return ls

rule target:
    input: 
        all_targets
    message: "Compiling all output"

 ##Level1   
if execution["star"]:
    include: "./snakemake_files/star_final.snakefile"
if execution["rseqc"]:
    include: "./snakemake_files/rseqc_final.snakefile"
if execution["salmon"]:
    include: "./snakemake_files/salmon_final.snakefile"
##Level2
if execution["batch_removal"]:
    include: "./snakemake_files/batch_removal.snakefile"
if execution["deseq2"]:
    include: "./snakemake_files/deseq2.modified.snakefile"
if execution["gsea"]:
    include: "./snakemake_files/gsea.modified.snakefile"
##Level3
if execution["fusion"]:
    include: "./snakemake_files/fusion.snakefile"
if execution["microbiota"]:
    include: "./snakemake_files/microbiota_final.snakefile"
if execution["trust4"]:
    include: "./snakemake_files/trust4_final.snakefile"
if execution["optitype"]:
    include: "./snakemake_files/optitype_final1.snakefile"
##Level4
if execution["tide"]:
    include: "./snakemake_files/tide.snakefile"
if execution["immunedeconv"]:
    include: "./snakemake_files/immunedeconv.snakefile"
if execution["wgcna"]:
    include: "./snakemake_files/wgcna_final.snakefile"
if execution["ssgsea"]:
    include: "./snakemake_files/ssgsea_final.snakefile"
if execution["msi_est"]:
    include: "./snakemake_files/msi_est.snakefile"
##Level5
if execution["varscan"]:   
    include: "./snakemake_files/varscan_final.snakefile"
if execution["vep"]: 
    include: "./snakemake_files/vep_final.snakefile"
if execution["pvacseq"]:
    include: "./snakemake_files/pvacseq_final.snakefile"
if execution["msisensor2"]:
    include: "./snakemake_files/msisensor2_final.snakefile"
if execution["lisa"]:
    include: "./snakemake_files/lisa_final.snakefile"

    













