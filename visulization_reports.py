#!/usr/bin/env python

#-------------------------------assign dynamic figures in report html-----------------------#
# @author: Jin Wang;
# @email: jwang0611@gmail.com
# @date: Oct, 2019

import pandas as pd
import yaml
import os.path

def load_config(config_file):
	with open(config_file, 'r') as stream:
		try:
			return yaml.safe_load(stream)
		except yaml.YAMLError as exc:
			print(exc)


config = load_config('config.yaml')

metadata = pd.read_csv(config["metasheet"], index_col=0, sep=',')
samples = list(metadata.index.values)   ###all the single samples
pats = []
for k, v in config["runs"].items(): ###all the tumor samples with paired reference
	if len(v) == 2:
		pats.append(k)

############################----------------config Django setting----------------############################
# template = [
# 	{
# 		'BACKEND': 'django.template.backends.django.DjangoTemplates',
# 		'DIRS': ['/Users/wangjin/Documents/rnaseq/bootstrap/templates'],
# 	}
# ]
# settings.configure(TEMPLATES=template)
# django.setup()
# from django.template.loader import get_template
# from django.template import Context

############################----------------replace contents in html----------------############################
def inplace_change_list(filename, old_string_list, new_string_list,output):
	# Safely read the input filename using 'with'
	with open(filename) as f:
		s = f.read()
		for old_string in old_string_list:
			if old_string not in s:
				print('"{old_string}" not found in {filename}.'.format(**locals()))
				return
	# Safely write the changed content, if found in the file
	output = open(output,"w")
	with open(filename, 'r') as f:
		s = f.read()
		for old_string in old_string_list:
			new_string = new_string_list[old_string_list.index(old_string)]
			print('Changing "{old_string}" to "{new_string}" in {filename}'.format(**locals()))
			s = s.replace(old_string, new_string)
		output.write(s)
	output.close()



##########################################################################################################################
##                                                                                                                      ##
##                                          RNASeq_Meta                                                               ##
##                                                                                                                      ##
##                                                                                                                      ##
##########################################################################################################################
###extract sequence information
read_length = config["read_length"]
data_path = config["data_path"]
assembly = config["assembly"]
library_type = config["library_type"]
plateform = config["RGPL"]
cancer_type = config["cancer_type"]
inplace_change_list('htmls/rnaseq_Meta_template.html',["ReadsLength","DataPath", "AssemblyType", "SequenceLibrary", "SequencePlatform", "CancerType"],
	[read_length, data_path, assembly, library_type, plateform, cancer_type],"htmls/tmp1.html")

############################----------------assign table from metasheet file----------------############################
with open("./metasheet.csv","r") as f:
	next(f)
	pat_list = {}
	batch_list = {}
	cond_list = {}
	for line in f:
		sample = line.rstrip("\n").split(",")[0]
		pat = line.rstrip("\n").split(",")[1]
		batch = line.rstrip("\n").split(",")[2]
		cond = line.rstrip("\n").split(",")[3]
		batch_list[sample] = batch
		cond_list[sample] = cond
		if pat in list(pat_list.keys()):
			pat_list[pat].append(sample)
		else:
			ss = []
			ss.append(sample)
			pat_list[pat] = ss
###replace content
template_meta_tab_span = """<tr><td rowspan="RS">Meta_Pat</td><td>Meta_Sample</td><td>Meta_Batch</td><td>Meta_Cond</td></tr>"""
template_meta_tab = """<tr><td>Meta_Sample</td><td>Meta_Batch</td><td>Meta_Cond</td></tr>"""
template_meta_tab_single = """<tr><td>Meta_Pat</td><td>Meta_Sample</td><td>Meta_Batch</td><td>Meta_Cond</td></tr>"""
table_meta = ""
for k, v in pat_list.items():
	rs = str(len(v))
	if(len(v) > 1):		
		for vv in v:
			if v.index(vv) == 0:
				temp1 = template_meta_tab_span.replace("RS", rs).replace("Meta_Pat", k).replace("Meta_Sample", vv).replace("Meta_Batch", batch_list[vv]).replace("Meta_Cond", cond_list[vv])
			else:
				temp1 = template_meta_tab.replace("Meta_Pat", k).replace("Meta_Sample", vv).replace("Meta_Batch", batch_list[vv]).replace("Meta_Cond", cond_list[vv])
			table_meta = table_meta + temp1
	else:
		vv = v[0]
		temp1 = template_meta_tab_single.replace("Meta_Pat", k).replace("Meta_Sample", vv).replace("Meta_Batch", batch_list[vv]).replace("Meta_Cond", cond_list[vv])
		table_meta = table_meta + temp1

print(table_meta)
############################Meta dynamic html
inplace_change_list('htmls/tmp1.html',["Metasheet_table"],[table_meta],"reports/rnaseq_Meta.html")


##########################################################################################################################
##                                                                                                                      ##
##                                          RNASeq_QC                                                                   ##
##                                                                                                                      ##
##                                                                                                                      ##
##########################################################################################################################
qc_option_template = """<option value ="singlesample">singlesample</option>"""
options_qc = ""
for ss in samples:
	temp = qc_option_template.replace("singlesample", ss)
	options_qc = options_qc + temp
inplace_change_list('htmls/rnaseq_QC_template.html',["qc_options_list", "firstsample"],[options_qc, samples[0]],"reports/rnaseq_QC.html")


##########################################################################################################################
##                                                                                                                      ##
##                                          RNASeq_Expression                                                              ##
##                                                                                                                      ##
##                                                                                                                      ##
##########################################################################################################################
############################----------------assign dynamic figures from DESeq2 and GSEA----------------############################
###exrtract all groups setting in wgcna
deseq2_loops = config["designs"]
############################deseq2
template_file_deseq2 = """
<a href="../images/deseq2/design/design_DESeq2_ConvertID.txt" download="../images/deseq2/design/design_DESeq2_ConvertID.txt" style="display:block;text-decoration:none;font-size:14px;"><li>design_DESeq2_result</li></a>       
<a href="../images/deseq2/design/design_diff_volcano_plot.png" download="../images/deseq2/design/design_diff_volcano_plot.png" style="display:block;text-decoration:none;font-size:14px;"><li>design_Volcano_plot</li></a>   
"""
template_volcano = """
<div class="col-6">
<h3 class="h5" style="text-align:center;color:blue">comparison in design</h3>   
<a href="../images/deseq2/design/design_diff_volcano_plot.png" ><img src="../images/deseq2/design/design_diff_volcano_plot.png" width="450" height="450" class="float-left"></a></div>
"""
############################GSEA
template_file_gsea = """
<a href="../images/deseq2/design/design_diff_gsea_plot.png" download="../images/deseq2/design/design_diff_gsea_plot.png" style="display:block;text-decoration:none;font-size:14px;"><li>design_GSEA_plot</li></a>
"""
template_gsea = """
<div class="col-12">
<h3 class="h5" style="text-align:center;color:blue">comparison in design</h3>   
<a href="../images/deseq2/design/design_diff_gsea_plot.png"><img src="../images/deseq2/design/design_diff_gsea_plot.png" width="900" height="700" class="float-left"></a></div>
"""
############################replace contents
files_deseq2 = ""
imgs_volcano = ""
files_gsea = ""
imgs_gsea = ""
for de in deseq2_loops:
	temp = template_file_deseq2.replace("design", de)
	files_deseq2 = files_deseq2 + temp
	temp1 = template_volcano.replace("design", de)
	imgs_volcano = imgs_volcano +temp1
	temp2 = template_file_gsea.replace("design", de)
	files_gsea = files_gsea +temp2
	temp3 = template_gsea.replace("design", de)
	imgs_gsea = imgs_gsea +temp3
############################DESeq2 dynamic html
inplace_change_list('htmls/rnaseq_expression_template.html',
	["deseq2_files","deseq2_design_loops", "gsea_files", "gsea_design_loops"],
	[files_deseq2,imgs_volcano,files_gsea,imgs_gsea],
	"htmls/tmp1.html")


############################----------------assign dynamic figures from ssGSEA----------------############################
###exrtract all groups setting in ssgsea
ssgsea_loops = config["ssgsea_comparisons"]
############################deseq2
template_file_ssgsea = """
<a href="images/ssgsea/clinic/clinic_ssgsea_plot.png" download="images/ssgsea/clinic/clinic_ssgsea_plot.png" style="display:block;text-decoration:none;font-size:14px;"><li>clinic_ssGSEA_plot</li></a> 
"""
template_ssgsea = """
<div class="col-6">
<h3 class="h5" style="text-align:center;color:blue">Comparison in clinic</h3>   
<a href="../images/ssgsea/clinic/clinic_ssgsea_plot.png"><img src="../images/ssgsea/clinic/clinic_ssgsea_plot.png" width="650" height="700" class="float-left"></a></div>
"""
############################replace contents
files_ssgsea = ""
imgs_ssgsea = ""
for cl in ssgsea_loops:
	temp = template_file_ssgsea.replace("clinic", cl)
	files_ssgsea = files_ssgsea + temp
	temp1 = template_ssgsea.replace("clinic", cl)
	imgs_ssgsea = imgs_ssgsea +temp1
############################ssGSEA dynamic html
inplace_change_list('htmls/tmp1.html',
	["ssGSEA_files","ssGSEA_clinic_loops"],[files_ssgsea,imgs_ssgsea],
	"htmls/tmp2.html")



############################----------------assign dynamic figures from WGCNA----------------############################
###exrtract all groups setting in wgcna
wgcna_loops = config["wgcna_clinical_phenotypes"]
###extract all phenotypes from all groups
treats_list = {}
for phenotype in wgcna_loops:
	treats = [i for i in list(set(metadata[phenotype].tolist()))]
	treats_list[phenotype] = treats
############################clinic_module_traits_plot
template_file_wgcna = """
<a href="../images/wgcna/clinic_module_traits_plot.png" download = "../images/wgcna/clinic_module_traits_plot.png" style="display:block;text-decoration:none;font-size:14px;"><li>clinic_module_traits_plot</li></a> 
<a href="../images/wgcna/clinic_phenotype_Module_Genes_plot.png" download = "../images/wgcna/clinic_phenotype_Module_Genes_plot.png" style="display:block;text-decoration:none;font-size:14px;"><li>clinic_phenotype_Module_Genes_plot</li></a>
<a href="../images/wgcna/clinic_phenotype_Connectivity_Genes_plot.png" download = "../images/wgcna/clinic_phenotype_Connectivity_Genes_plot.png" style="display:block;text-decoration:none;font-size:14px;"><li>clinic_phenotype_Connectivity_Genes_plot</li></a>
<a href="../images/wgcna/clinic_phenotype_Connectivity_Genes_plot.png" download = "../images/wgcna/clinic_phenotype_Connectivity_Genes_plot.png" style="display:block;text-decoration:none;font-size:14px;"><li>clinic_phenotype_Connectivity_Genes_plot</li></a>
<a href="../files/wgcna/clinic_phenotype_hubgenes.txt" download = "../files/wgcna/clinic_phenotype_hubgenes.txt" style="display:block;text-decoration:none;font-size:14px;"><li>clinic_phenotype_hubgenes</li></a>
"""
template_clinic = """
<h3 class="h5" style="text-align:center;color:blue">clinic</h3>
<a href="../images/wgcna/clinic_module_traits_plot.png"><img src="../images/wgcna/clinic_module_traits_plot.png" width="650" height="650" class="float-left"></a> 
"""
template_phenotype = """<div class="col-12">
<h3 class="h5" style="text-align:center;color:blue">clinic: phenotype</h3></div>
<div class="col-6"><a href="../images/wgcna/clinic_phenotype_Module_Genes_plot.png"><img src="../images/wgcna/clinic_phenotype_Module_Genes_plot.png" width="450" height="450" class="float-left"></a></div>
<div class="col-6"><a href="../images/wgcna/clinic_phenotype_Connectivity_Genes_plot.png"><img src="../images/wgcna/clinic_phenotype_Connectivity_Genes_plot.png" width="450" height="450" class="float-left"></a></div> 
"""
###########################replace contents
imgs_wgcna1 = ""
imgs_wgcna2 = ""
files_wgcna = ""
for k,v in treats_list.items():
	temp = template_clinic.replace("clinic", cl)
	imgs_wgcna1 = imgs_wgcna1 + temp
	for tr in v:
		temp1 = template_phenotype.replace("clinic", k).replace("phenotype", tr)
		imgs_wgcna2 = imgs_wgcna2 + temp1
		temp2 = template_file_wgcna.replace("clinic", k).replace("phenotype", tr)
		files_wgcna = files_wgcna + temp2
############################WGCNA dynamic html
# inplace_change('rnaseq_expression_template.html', "wgcna_clinical_loops", imgs_wgcna1, "tmp.html")
# inplace_change('tmp.html', "wgcna_phenotype_loops", imgs_wgcna2, "rnaseq_Expression.html")
inplace_change_list('htmls/tmp2.html',
	["wgcna_files","wgcna_clinical_loops","wgcna_phenotype_loops"],[files_wgcna,imgs_wgcna1,imgs_wgcna2],
	'htmls/tmp3.html')


############################----------------assign dynamic figures from lisa----------------############################
###exrtract all groups setting in lisa
lisa_loops = config["designs"]
############################clinic_module_traits_plot
template_img_lisa = """
<h3 class="h5" style="text-align:center;color:blue">clinic: transcriptional factors targeted on DEGs</h3>
<a href="../images/lisa/clinic/clinic_lisa_up_down_TF.png"><img src="../images/lisa/clinic/clinic_lisa_up_down_TF.png" width="650" height="700" class="float-left"></a>
"""
template_file_lisa = """
<a href="../images/lisa/clinic/clinic_lisa_up_down_TF.png" download = "../images/lisa/clinic/clinic_lisa_up_down_TF.png" style="display:block;text-decoration:none;font-size:14px;"><li>clinic_TF_regulate_DEG_plot</li></a> 
"""
###########################replace contents
imgs_lisa = ""
files_lisa = ""
for l in lisa_loops:
	temp1 = template_img_lisa.replace("clinic", l)
	imgs_lisa = imgs_lisa + temp1
	temp2 = template_file_lisa.replace("clinic", l)
	files_lisa = files_lisa + temp2
############################LISA dynamic html
inplace_change_list('htmls/tmp3.html',
	["LISA_clinic_loops", "LISA_download_files"],[imgs_lisa, files_lisa],
	"reports/rnaseq_Expression.html")





##########################################################################################################################
##                                                                                                                      ##
##                                          RNASeq_Mutation                                                             ##
##                                                                                                                      ##
##                                                                                                                      ##
##########################################################################################################################
############################----------------assign dynamic figures from Varscan----------------############################
template_file_maf = """<tr><td>MAF_Pat</td><td>MAF_Tumor</td><td>MAF_Symbol</td><td>MAF_Chr</td><td>MAF_Start</td><td>MAF_End</td><td>MAF_Strand</td><td>MAF_VC</td><td>MAF_VT</td><td>MAF_Ref</td><td>MAF_Allele1</td><td>MAF_Allele2</td><td>MAF_HGVSc</td><td>MAF_Short</td></tr>"""
###read in maf data and replace content
table_maf = ""
try:
	with open("images/variant/cohort/maftools_table.txt","r") as f:
		next(f)
		for line in f:
			patient = line.split("\t")[0]
			tumor = line.split("\t")[1]
			symbol = line.split("\t")[3]
			chrome = line.split("\t")[5]
			start = line.split("\t")[6]
			end = line.split("\t")[7]
			strand = line.split("\t")[8]
			vc = line.split("\t")[9]
			vt = line.split("\t")[10]
			ref = line.split("\t")[11]
			allele1 = line.split("\t")[12]
			allele2 = line.split("\t")[13]
			sc = line.split("\t")[14]
			short = line.split("\t")[16]
			temp = template_file_maf.replace("MAF_Pat", patient).replace("MAF_Tumor", tumor).replace("MAF_Symbol", symbol).replace("MAF_Chr", chrome).replace("MAF_Start", start).replace("MAF_End", end).replace("MAF_Strand", strand).replace("MAF_VC", vc).replace("MAF_VT", vt).replace("MAF_Ref", ref).replace("MAF_Allele1", allele1).replace("MAF_Allele2", allele2).replace("MAF_HGVSc", sc).replace("MAF_Short", short)
			table_maf = table_maf + temp
except IOError:
	print ("images/variant/cohort/maftools_table.txt not exist")
############################MSI dynamic html
inplace_change_list('htmls/rnaseq_Mutation_template.html',["maf_table_rows"],[table_maf],"htmls/tmp1.html")

############################----------------assign dynamic figures from Fusion----------------############################
###exrtract all groups setting in fusion
fusion_loops = config["fusion_clinical_phenotypes"]
###extract all phenotypes from all groups
treats_list = {}
for phenotype in fusion_loops:
	treats = [i for i in list(set(metadata[phenotype].tolist()))]
	treats_list[phenotype] = treats
############################clinic_phenotype_fusion_circle_plot
template_file_fusion = """
<a href="../images/fusion/clinic_phenotype_fusion_circle_plot.png" download="../images/fusion/clinic_phenotype_fusion_circle_plot.png" style="display:block;text-decoration:none;font-size:14px;"><li>clinic_phenotype_circos_plot</li></a>  
<a href="../images/fusion/filter_fusion_clinic_phenotype.txt" download="../images/fusion/filter_fusion_clinic_phenotype.txt" style="display:block;text-decoration:none;font-size:14px;"><li>clinic_phenotype_fusion_result.txt</li></a>  
"""
template_fusion = """
<div class="col-6">
<h3 class="h5" style="text-align:center">clinic: phenotype</h3>                   
<a href="../images/fusion/clinic_phenotype_fusion_circle_plot.png"><img src="../images/fusion/clinic_phenotype_fusion_circle_plot.png" width="700" height="700" class="float-left"></a></div>
"""
###########################replace contents
imgs_fusion = ""
files_fusion = ""
for k,v in treats_list.items():
	for tr in v:
		temp = template_fusion.replace("clinic", k).replace("phenotype", tr)
		imgs_fusion = imgs_fusion + temp
		temp1 = template_file_fusion.replace("clinic", k).replace("phenotype", tr)
		files_fusion = files_fusion + temp1
############################FUSION dynamic html
inplace_change_list('htmls/tmp1.html',["fusion_files","fusion_loops"],[files_fusion,imgs_fusion],"htmls/tmp2.html")


############################----------------assign table from msisensor----------------############################
template_file_msi = """<tr><td>MIS_Sample</td><td>MSI_Score</td></tr>"""
###read in msi score data and replace content
table_msi = ""
try:
	with open("files/msisensor/msi_score.txt","r") as f:
		for line in f:
			sample = line.rstrip().split("\t")[0]
			score = line.rstrip().split("\t")[1]
			temp = template_file_msi.replace("MIS_Sample", sample).replace("MSI_Score", score)
			table_msi = table_msi + temp
except IOError:
	print ("files/msisensor/msi_score.txt not exist")
############################MSI dynamic html
inplace_change_list('htmls/tmp2.html',["msi_score_table_rows"],[table_msi],"htmls/tmp3.html")

############################----------------assign option list of mutation----------------############################
mutation_option_template = """<option value ="singlesample">singlesample</option>"""
options_mutation = ""
for ss in pats:
	temp = qc_option_template.replace("singlesample", ss)
	options_mutation = options_mutation + temp
inplace_change_list('htmls/tmp3.html',["mutation_options_list", "firstsample"],[options_mutation, samples[0]],"reports/rnaseq_Mutation.html")



##########################################################################################################################
##                                                                                                                      ##
##                                          RNASeq_Immune                                                               ##
##                                                                                                                      ##
##                                                                                                                      ##
##########################################################################################################################
############################----------------assign table from optitype----------------############################
template_file_optitype = """<tr><td>Optitype_Sample</td><td>Optitype_A1</td><td>Optitype_A2</td><td>Optitype_B1</td><td>Optitype_B2</td><td>Optitype_C1</td><td>Optitype_C2</td></tr>"""
###read in msi score data and replace content
table_optitype = ""
try:
	with open("files/optitype/merged_HLAI_typing.txt","r") as f:
		next(f)
		for line in f:
			sample = line.rstrip().split("\t")[0]
			A1 = line.rstrip().split("\t")[1]
			A2 = line.rstrip().split("\t")[2]
			B1 = line.rstrip().split("\t")[3]
			B2 = line.rstrip().split("\t")[4]
			C1 = line.rstrip().split("\t")[5]
			C2 = line.rstrip().split("\t")[6]
			temp = template_file_optitype.replace("Optitype_Sample", sample).replace("Optitype_A1", A1).replace("Optitype_A2", A2).replace("Optitype_B1", B1).replace("Optitype_B2", B2).replace("Optitype_C1", C1).replace("Optitype_C2", C2)
			table_optitype = table_optitype + temp
except IOError:
	print ("files/optitype/merged_HLAI_typing.txt not exist")
###Optitype dynamic html
inplace_change_list('htmls/rnaseq_Immune_template.html',["HLA_table_rows"],[table_optitype],"htmls/tmp1.html")


############################----------------assign table from pvacseq----------------############################
template_file_pvacseq = """<tr><td>PVAC_Sample</td><td>PVAC_Symbol</td><td>PVAC_MutP</td><td>PVAC_Pos</td><td>PVAC_SC</td><td>PVAC_SP</td><td>PVAC_HLA</td><td>PVAC_Epitope</td><td>PVAC_MutIC50</td><td>PVAC_WTIC50</td><td>PVAC_FC</td><td>PVAC_Expression</td></tr>"""
###read in pvacseq prediction and replace content
table_pvacseq = ""
try:
	with open("files/pvacseq/Merged.filtered.condensed.ranked.addSample.tsv","r") as f:
		next(f)
		for line in f:
			sample = line.rstrip().split("\t")[0]
			symbol = line.rstrip().split("\t")[1]
			mut_protein = line.rstrip().split("\t")[2]
			position = line.rstrip().split("\t")[3]
			sc = line.rstrip().split("\t")[4]
			sp = line.rstrip().split("\t")[5]
			hla = line.rstrip().split("\t")[6]
			epitope = line.rstrip().split("\t")[7]
			mut_IC50 = line.rstrip().split("\t")[8]
			wt_IC50 = line.rstrip().split("\t")[9]
			fc  = line.rstrip().split("\t")[10]
			expression = line.rstrip().split("\t")[15]
			temp = template_file_pvacseq.replace("PVAC_Sample", sample).replace("PVAC_Symbol", symbol).replace("PVAC_MutP", mut_protein).replace("PVAC_Pos", position).replace("PVAC_SC", sc).replace("PVAC_SP", sp).replace("PVAC_Epitope", epitope).replace("PVAC_MutIC50", mut_IC50).replace("PVAC_WTIC50", wt_IC50).replace("PVAC_FC", fc).replace("PVAC_Expression", expression)
			table_pvacseq = table_pvacseq + temp
except IOError:
	print ("files/pvacseq/Merged.filtered.condensed.ranked.addSample.tsv not exist")
###Pvacseq dynamic html
inplace_change_list('htmls/tmp1.html',["PVACSeq_table"],[table_pvacseq],"htmls/tmp2.html")

############################----------------assign option list of immune----------------############################
immune_option_template = """<option value ="singlesample">singlesample</option>"""
options_immune = ""
for ss in samples:
	temp = immune_option_template.replace("singlesample", ss)
	options_immune = options_immune + temp
inplace_change_list('htmls/tmp2.html',["immune_options_list", "firstsample"],[options_immune, samples[0]],"reports/rnaseq_Immune.html")


##########################################################################################################################
##                                                                                                                      ##
##                                          RNASeq_Microbiota                                                           ##
##                                                                                                                      ##
##                                                                                                                      ##
##########################################################################################################################
############################----------------assign table from centrifuge----------------############################
microbes_loops = config["microbiota_clinical_phenotypes"]
template_microbe1 = """<div class="col-12">
<a href="../images/microbiota/clinic_microbes_abundance.png"><img src="../images/microbiota/clinic_microbes_abundance.png" width="700" height="700" style="padding-left: 10px" class="float-left"></a>
</div>
"""
template_microbe2 = """<div class="col-12">
<a href="../images/microbiota/clinic_phenotype_microbes_clustering.png"><img src="../images/microbiota/clinic_phenotype_microbes_clustering.png" width="600" height="720" style="padding-left: 10px" class="float-left"></a>
</div>
"""
###extract all phenotypes from all groups
treats_list = {}
for phenotype in microbes_loops:
	treats = [i for i in list(set(metadata[phenotype].tolist()))]
	treats_list[phenotype] = treats

###########################replace contents
imgs_microbe1 = ""
imgs_microbe2 = ""
for k,v in treats_list.items():
	temp = template_microbe1.replace("clinic", k)
	imgs_microbe1 = imgs_microbe1 + temp
	for tr in v:
		temp1 = template_microbe2.replace("clinic", k).replace("phenotype", tr)
		imgs_microbe2 = imgs_microbe2 + temp1
############################FUSION dynamic html
inplace_change_list('htmls/rnaseq_Microbiota_template.html',["Clinical_microbe_abundance","Phenotype_clustering_abundance"],[imgs_microbe1, imgs_microbe2],"reports/rnaseq_Microbiota.html")














