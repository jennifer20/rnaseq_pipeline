#!/bin/bash
ref=(bed.tar.gz star_gdc_index.tar.gz centrifuge_index.tar.gz lisa.tar.gz trust4.tar.gz msisensor_gdc_index.tar.gz fasta.tar.gz pathseq_gdc_index.tar.gz vep.tar.gz fusion_gdc_index.tar.gz rsem_gdc_index.tar.gz salmon_gdc_index.tar.gz)
wget http://cistrome.org/~jinwang/reference/gencode.v22.annotation.gtf
for i in ${ref[@]}
do
wget http://cistrome.org/~jinwang/reference/${i}
tar -zxvf ${i}
done


