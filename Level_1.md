# RNAseq_pipeline
## 1. Config.yaml

### 1.1 General setting
The `metasheet` parameter specifies the metadata file name and parameter `meta` specifies sequencing data type (paired-end or single-end).
```
metasheet: metasheet.csv     #[Required.]
mate: [1,2]                  #[Required.] Paired-end = [1,2] and single-end = [0]
ref: ref.yaml                #[Required.] reference file name
assembly: hg38               #[Required.] reference genome
```
### 1.2 Samples
The `Samples` parameter is required and specifies the location of raw fastq data of each sample.
```
samples:    #[Required.]
  R19013702LR01-150521T:
    - /mnt/pilot3_rna/test/rnasq_final/data/pair/R19013702LR01-150521T/R19013702LR01-150521T_combined_R1.fastq.gz
    - /mnt/pilot3_rna/test/rnasq_final/data/pair/R19013702LR01-150521T/R19013702LR01-150521T_combined_R2.fastq.gz
  R18074855LR01-150521N:
    - /mnt/pilot3_rna/test/rnasq_final/data/pair/R18074855LR01-150521N/R18074855LR01-150521N_combined_R1.fastq.gz
    - /mnt/pilot3_rna/test/rnasq_final/data/pair/R18074855LR01-150521N/R18074855LR01-150521N_combined_R2.fastq.gz
  R19013727LR01-181874T:
    - /mnt/pilot3_rna/test/rnasq_final/data/data_bk/single/R19013727LR01-181874T/R19013727LR01-181874T_combined_R1.fastq.gz
    - /mnt/pilot3_rna/test/rnasq_final/data/data_bk/single/R19013727LR01-181874T/R19013727LR01-181874T_combined_R2.fastq.gz
```
### 1.3 Runs
The `Runs` parameter is required and specifies the tumor/normal pair of each run.
```
runs:    #[Required.]
  run1: ##add matched normal-Tumor samples in one run
    - R19013702LR01-150521T
    - R18074855LR01-150521N
  run2: ##add only tumor sample if no matched normal samples
    - R19013727LR01-181874T
```
### 1.4 Level 1
In the Level 1 of our NRA-seq pipeline, we conduct __read alignment__, __quality control__ and __gene quantification analysis__.

### Software Version
- STAR v2.6.1d
- RSEM v1.3.1
- Salmon v1.1.0
- RSeQC v2.6.4
---
#### 1.4.1 Read alignment
We use STAR to align the RNA-seq reads to the reference genome `GRCh38.d1.vd1.fa` downloaded from GDC and also calculate the raw transcripts per million (TPM) of all samples. The `library_type` parameter refers to the library prep method.

```
library_type: fr-firststrand    #[Optional.] Other options can be [fr-firststrand, fr-secondstrand].
```

##### Example outputs

```
analysis/star/
├── R18074855LR01-150521N
│   ├── R18074855LR01-150521N.Chimeric.out.junction
│   ├── R18074855LR01-150521N.Log.final.out
│   ├── R18074855LR01-150521N.counts.tab
│   ├── R18074855LR01-150521N.sorted.bam
│   ├── R18074855LR01-150521N.sorted.bam.bai
│   ├── R18074855LR01-150521N.sorted.bam.stat.txt
│   ├── R18074855LR01-150521N.sorted.driver.extract.bam
│   ├── R18074855LR01-150521N.transcriptome.bam
│   ├── R18074855LR01-150521N.unsorted.bam
│   ├── R18074855LR01-150521NLog.out
│   ├── R18074855LR01-150521NLog.progress.out
│   ├── R18074855LR01-150521NSJ.out.tab
│   ├── R18074855LR01-150521N_STARgenome
│   │   ├── sjdbInfo.txt
│   │   └── sjdbList.out.tab
│   ├── R18074855LR01-150521N_STARpass1
│   │   ├── Log.final.out
│   │   └── SJ.out.tab
│   ├── R18074855LR01-150521N_downsampling.bam
│   └── R18074855LR01-150521N_downsampling.bam.bai
├── R19013702LR01-150521T
│   ├── R19013702LR01-150521T.Chimeric.out.junction
│   ├── R19013702LR01-150521T.Log.final.out
│   ├── R19013702LR01-150521T.counts.tab
│   ├── R19013702LR01-150521T.sorted.bam
│   ├── R19013702LR01-150521T.sorted.bam.bai
│   ├── R19013702LR01-150521T.sorted.bam.stat.txt
│   ├── R19013702LR01-150521T.sorted.driver.extract.bam
│   ├── R19013702LR01-150521T.transcriptome.bam
│   ├── R19013702LR01-150521T.unsorted.bam
│   ├── R19013702LR01-150521TAligned.sortedByCoord.out.bam
│   ├── R19013702LR01-150521TLog.out
│   ├── R19013702LR01-150521TLog.progress.out
│   ├── R19013702LR01-150521TSJ.out.tab
│   ├── R19013702LR01-150521T_STARgenome
│   │   ├── sjdbInfo.txt
│   │   └── sjdbList.out.tab
│   ├── R19013702LR01-150521T_STARpass1
│   │   ├── Log.final.out
│   │   └── SJ.out.tab
├── R19013702LR01-150521T_downsampling.bam
│   └── R19013702LR01-150521T_downsampling.bam.bai
├── R19013727LR01-181874T
│   ├── R19013727LR01-181874T.Chimeric.out.junction
│   ├── R19013727LR01-181874T.Log.final.out
│   ├── R19013727LR01-181874T.counts.tab
│   ├── R19013727LR01-181874T.sorted.bam
│   ├── R19013727LR01-181874T.sorted.bam.bai
│   ├── R19013727LR01-181874T.sorted.bam.stat.txt
│   ├── R19013727LR01-181874T.transcriptome.bam
│   ├── R19013727LR01-181874T.unsorted.bam
│   ├── R19013727LR01-181874TLog.out
│   ├── R19013727LR01-181874TLog.progress.out
│   ├── R19013727LR01-181874TSJ.out.tab
│   ├── R19013727LR01-181874T_STARgenome
│   │   ├── sjdbInfo.txt
│   │   └── sjdbList.out.tab
│   ├── R19013727LR01-181874T_STARpass1
│   │   ├── Log.final.out
│   │   └── SJ.out.tab
│   ├── R19013727LR01-181874T_downsampling.bam
│   └── R19013727LR01-181874T_downsampling.bam.bai
└── STAR_Gene_Counts.csv
```

---
#### 1.4.2 Quality check

We use RSeQC to conduct quality control. We also provide an option to check the quality using our __[house keeping genes](https://www.nature.com/articles/s41596-018-0113-7)__.
```
rseqc_ref: housekeeping    #[Optional.] If not provided, only down-sampled alignment will be used.
```

##### Example outputs

```
analysis/rseqc
├── R18074855LR01-150521N
│   ├── R18074855LR01-150521N.stat_tmp.txt
│   ├── R18074855LR01-150521N_downsampling_housekeeping.bam
│   └── R18074855LR01-150521N_downsampling_housekeeping.bam.bai
├── R19013702LR01-150521T
│   ├── R19013702LR01-150521T.stat_tmp.txt
│   ├── R19013702LR01-150521T_downsampling_housekeeping.bam
│   └── R19013702LR01-150521T_downsampling_housekeeping.bam.bai
├── R19013727LR01-181874T
│   ├── R19013727LR01-181874T.stat_tmp.txt
│   ├── R19013727LR01-181874T_downsampling_housekeeping.bam
│   └── R19013727LR01-181874T_downsampling_housekeeping.bam.bai
|── gene_body_cvg
│   └── geneBodyCoverage.r
├── read_distrib
│   ├── R18074855LR01-150521N
│   │   └── R18074855LR01-150521N.txt
│   ├── R19013702LR01-150521T
│   │   └── R19013702LR01-150521T.txt
│   ├── R19013727LR01-181874T
│   │   └── R19013727LR01-181874T.txt
└── tin_score
    ├── R18074855LR01-150521N
    │   ├── R18074855LR01-150521N.summary.txt
    │   └── R18074855LR01-150521N.tin_score.txt
    ├── R19013702LR01-150521T
    │   ├── R19013702LR01-150521T.summary.txt
    │   └── R19013702LR01-150521T.tin_score.txt
    ├── R19013727LR01-181874T
    │   ├── R19013727LR01-181874T.summary.txt
    │   └── R19013727LR01-181874T.tin_score.txt
```

---
#### 1.4.3 Gene quantification
We use RSEM or Salmon to quantify the expression of transcripts of RNA-seq data. The parameter `rsem` can be set at true or false. If set to false, Salmon was used for transcript quantification.

- Option 1: Use __Salmon__.
```
rsem: false   #[Required.] true or false
```

##### Example outputs
```
analysis/salmon
├── R18074855LR01-150521N
│   ├── R18074855LR01-150521N.quant.sf
│   ├── R18074855LR01-150521N.transcriptome.bam.log
│   ├── aux_info
│   │   ├── ambig_info.tsv
│   │   ├── expected_bias.gz
│   │   ├── fld.gz
│   │   ├── meta_info.json
│   │   ├── observed_bias.gz
│   │   └── observed_bias_3p.gz
│   ├── cmd_info.json
│   ├── libParams
│   └── logs
│       └── salmon_quant.log
├── R19013702LR01-150521T
│   ├── R19013702LR01-150521T.quant.sf
│   ├── R19013702LR01-150521T.transcriptome.bam.log
│   ├── aux_info
│   │   ├── ambig_info.tsv
│   │   ├── expected_bias.gz
│   │   ├── fld.gz
│   │   ├── meta_info.json
│   │   ├── observed_bias.gz
│   │   └── observed_bias_3p.gz
│   ├── cmd_info.json
│   ├── libParams
│   └── logs
│       └── salmon_quant.log
├── R19013727LR01-181874T
│   ├── R19013727LR01-181874T.quant.sf
│   ├── R19013727LR01-181874T.transcriptome.bam.log
│   ├── aux_info
│   │   ├── ambig_info.tsv
│   │   ├── expected_bias.gz
│   │   ├── fld.gz
│   │   ├── meta_info.json
│   │   ├── observed_bias.gz
│   │   └── observed_bias_3p.gz
│   ├── cmd_info.json
│   ├── libParams
│   └── logs
│       └── salmon_quant.log
└── salmon_tpm_matrix.csv
```


- Option 2: Use __RSEM__.
```
rsem: true   #[Required.]
stranded: false #[Optional.]
```

## 2. metasheet.csv
The metasheet file saves clinical information of RNA-seq samples. First four column is required for our pipeline and other columns (which might be used for 'batch removal' using __limma__ in level 2) can also be added.

- Column 1: __SampleName__ is required, which specifies CIMAC id or user defined sample name
- Column 2: __PatName__ is required, which specifies tumor-normal pair information and corresponding to `Runs` in 'Config,yaml' file.
- Column 3: __Batch__ is required, which specifies batch information through experiments, this column is used in 'batch removal' using __combat__.
- Column 4: __Tissue__ is required, which specifys tumor or normal sample.

```
SampleName,PatName,Batch,Tissue,Treatment
R19013702LR01-150521T,run1,1,tumor,Post-treatment
R18074855LR01-150521N,run1,2,normal,Pre-treatment
R19013727LR01-181874T,run2,1,tumor,Post-treatment

```


