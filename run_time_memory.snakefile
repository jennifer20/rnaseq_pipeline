#!/usr/bin/env python

#------------------------Run time memory-----------------------#
# @author: Jin Wang @ref:VIPER
# @email: jwang0611@gmail.com

import yaml

with open("execution.yaml","r") as stream:
    execution=yaml.safe_load(stream)

###include benchmarks in time-memory summary based on users' execution requiremnet
files=[]
modules=[]
if execution["star"]:
    files.append("star_align.benchmark")
    modules.append("star")
if execution["gene_quantification"]:
    files.append("quantification.benchmark")
    modules.append("salmon")
if execution["varscan"]:   
    files.append("varscan.benchmark")
    modules.append("varscan")
if execution["rseqc"]: 
    files.append("rseqc_genebody_cvg.benchmark")
    files.append("rseqc_read_qual.benchmark")
    files.append("rseqc_read_distrib.benchmark")
    files.append("rseqc_tin_score.benchmark")
    files.append("rseqc_insert_size.benchmark")
    files.append("rseqc_junction_saturation.benchmark")
    modules.append("rseqc")
if execution["vep"]: 
    files.append("vep.benchmark")
    modules.append("vep")
if execution["optitype"]:
    files.append("optitype.benchmark")
    modules.append("optitype")
if execution["pvacseq"]:
    files.append("pvacseq.benchmark")
    modules.append("pvacseq")
if execution["msisensor2"]:
    files.append("msisensor.benchmark")
    modules.append("msisensor")
if execution["fusion"]:
    files.append("fusion.benchmark")
    modules.append("fusion")
if execution["microbiota"]:
    files.append("microbiota.benchmark")
    modules.append("centrifuge")
if execution["msi_est"]:
    files.append("msi_est_score.benchmark")
    modules.append("msi_est")
if execution["tide"]:
    files.append("tide_score.benchmark")
    modules.append("tide")
if execution["immunedeconv"]:
    files.append("deconv.benchmark")
    modules.append("immunedeconv")
if execution["wgcna"]:
    files.append("WGCNA.benchmark")
    modules.append("wgcna")
if execution["deseq2"]:
    files.append("deseq2.benchmark")
    modules.append("deseq2")
if execution["ssgsea"]:
    files.append("ssgsea.benchmark")
    modules.append("ssgsea")
if execution["lisa"]:
    files.append("lisa.benchmark")
    modules.append("lisa")
if execution["trust4"]:
    files.append("trust4.benchmark")
    modules.append("trust4")
# print(files)
# print(modules)   

def run_time_memory_targets(wildcards):
    ls=[]
    ls.append("images/consumption/run_time_memory.png")
    for file in files:
        ls.append("benchmarks/consumption/%s" % file)
    print(ls)
    return ls

rule run_all:
    input:
        run_time_memory_targets

rule benchmark_collect:
    input:
        expand("benchmarks/{module}",module=modules)
        # ["benchmarks/{module}".format(module=module) for module in modules]
    output:
        expand("benchmarks/consumption/{file}",file=files)
        # ["benchmarks/consumption/{file}".format(file=file) for file in files]
    shell:
        "/bin/bash src/benchmark_collect.sh"

rule bechmark_plot:
    input:
        expand("benchmarks/consumption/{file}",file=files)
        # ["benchmarks/consumption/{file}".format(file=file) for file in files]
    output:
        "images/consumption/run_time_memory.png"
    params:
        input=lambda wildcards, input: ",".join(str(i) for i in list(input))
    conda: "envs/stat_perl_r.yml"
    shell:
        "Rscript src/run_time_memory.R --input {params.input}"