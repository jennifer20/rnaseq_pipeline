#!/usr/bin/env python

from genson import SchemaBuilder

###############################################################################
# Constants
###############################################################################
_int = 777
_float = 7.0
_string = "TGBTG"
_int_arr = [_int,_int,_int]
_file_path = "/the/only/way"

###############################################################################
# SAMPLE level definition
###############################################################################
sample = {'id': _string, #CIAMC id
		  'cancer':_string,
 		  'patient': _string,#patient id
          'Tissue': _string, 
          'Status': _string, #tumor or normal
          'Outcome':_string, #Optional
          'Treatment':_string} #pre or post treatment
###############################################################################
# END SAMPLE level definition
###############################################################################


###############################################################################
# Alignment and quality control level definition
###############################################################################
          
alignment = {'total_reads': _int,
             'mapped_reads': _int,
             'dedup_reads': _int,
             'quality_score': _int_arr,
             'bam':_file_path, #path to sorted bam
             'bam_stat':_file_path,
             'bam_downsample':_file_path, #path to downsampled bam
             'bam_transcript':_file_path, #path to transcript bam
             'bam_housekeeping':_file_path, #OPTIONAL path to housekeeping bam
             'log':_file_path, #path to log file for report
             'counts':_file_path, #path to count file for report
             'junction':_file_path} 

quality = {'tin': _file_path,
           'medTin': _float,
           'read_distribute': _file_path,
           'gene_body_cov':_file_path,
           'gene_bofy_cov_img':_file_path,
           'quality_box':_file_path,
           'quality_heatmap':_file_path,
           'insert_size':_file_path,
           'junction_satu':_file_path}
           
fusion = {'fusion':_file_path}
###############################################################################
# END Alignment and quality control level definition
###############################################################################

###############################################################################
# Microbiome level definition
###############################################################################
microb = {'report':_file_path,
              'classification':_file_path}
###############################################################################
# END Microbiome level  definition
###############################################################################


###############################################################################
# Immune repertoire level definition
###############################################################################
repertoire = {'cdr3':_file_path,
              'report':_file_path,
              'rdata':_file_path,
              'individual_plot':_file_path}
###############################################################################
# END Immune repertoire  definition
###############################################################################

###############################################################################
# Immune infiltration level definition
###############################################################################
infiltration = {'dysfunction':_float,
                'exclusion':_float,
                'tide_score':_float,
                'MDSC':_float,
                'CAF':_float,
                'TAM':_float,
                'M2':_float}
###############################################################################
# END Immune repertoire  definition
###############################################################################


###############################################################################
# MSI level definition
###############################################################################
msi = {'score':_float}
###############################################################################
# END MSI definition
###############################################################################



###############################################################################
# neoantigen level definition
###############################################################################

hla = { "A-1": _string,
        "A-2": _string,
        "B-1": _string,
        "B-2": _string,
        "C-1": _string,
        "C-2": _string,
        "DPB1-1": _string, #OPTIONAL
        "DPB1-2": _string, #OPTIONAL
        "DQB1-1": _string, #OPTIONAL
        "DQB1-2": _string, #OPTIONAL
        "DRB1-1": _string, #OPTIONAL
        "DRB1-2": _string} #OPTIONAL

mutation_results = {"total": _int,
                    "snp": _int,
                    "insertion": _int,
                    "deletion": _int}

transition_row = {"A": _int,
                  "C": _int,
                  "G": _int,
                  "T": _int}

somatic_results = {'filtered_vcf_file': _file_path,
                   'filtered_maf_file': _file_path,
                   'tmb': _int, #Total mutation burden
                   'mutation_summary': mutation_results,
                   'functional_summary': mutation_results,
                   'trinucleotide_matrix': _int_arr,
                   'transition_matrix': {'A': transition_row,
                                         'C': transition_row,
                                         'G': transition_row,
                                         'T': transition_row}}
                                         
neoantigen_row = {"Gene": _string,
                  "EnsemblID": _string,
                  "HLA": _string,
                  "Peptide_Sequence": _string,
                  "Read_Depth": _float,
                  "DNA_VAF": _float,
                  "Method": _string,
                  "Score": _float,
                  "WT_Score": _float,
                  "Fold_Change": _float}

###############################################################################
# END neoantigen level definition
###############################################################################

###############################################################################
# RUN level definition
###############################################################################

run = {'meta': sample,
       'align': [alignment,fusion],
       'QC':quality,
       'microbiome': microb,
       'somatic': somatic_results,
       'HLA': hla,
       'neoantigen': [neoantigen_row, neoantigen_row, neoantigen_row],
       'neoantigen_file': _file_path, #path to pvacseq filtered.tsv file
       'msi_score': msi, 
       'immune_repertoire': repertoire, 
       'immune_infiltration': infiltration
       }

###############################################################################
# END RUN level definition
###############################################################################

builder = SchemaBuilder(schema_uri="http://json-schema.org/draft-07/schema#")
builder.add_object(run)
print(builder.to_json(indent=3))

