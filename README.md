# rnaseq_pipeline
## 1. Data preparation
Before we start with the pipeline, we need to prepare the data and setup the configuration. Create “project” folder for running this pipeline. Prepare your data in a folder called “data”, noting that put your samples into "pair" sub-folder if providing tumor-normal paired samples or "single" sub-folder if providing tumor samples only.
```shell
#Command:
mkdir project
cd project
mkdir data reference
cd data
mkdir pair single
```
### 1.1 Sequencing data preparation
In your “data” folder, the structure should be like this:
```
├── pair
│   ├── R18074784LR01-160006T
│   ├── R18074860LR01-160006N
│   ├── R19013702LR01-150521T
│   ├── R18074855LR01-150521N
│   ├── …
│   └── …
└── single
    ├── R19013710LR01-170358T
    ├── R19013713LR01-171439T
    ├── …
```
Put your single-end or pair-end fastq file in each sample folder:
```
├── pair
│   ├── R18074778LR01-150513T
│   │   ├── R18074778LR01-150513T_combined_R1.fastq.gz
│   │   └── R18074778LR01-150513T_combined_R2.fastq.gz
│   ├── R18074784LR01-160006T
│   │   ├── R18074784LR01-160006T_combined_R1.fastq.gz
│   │   └── R18074784LR01-160006T_combined_R2.fastq.gz
```
### 1.2 Reference data preparation
We used Gencode V22 annotation files, based on which we built index for some tools. You can directly download them into the "reference" folder made previously from [here](http://cistrome.org/~jinwang/reference/). Cause some files are extremely large, we provide compressed files. You need uncompress them after downloading. If you want to execute part of the pipeline, just dowanload what you need. Here we take star_gdc_index as an example to show you how to download and uncompress.
```shell
wget http://cistrome.org/~jinwang/reference/star_gdc_index.tar.gz
tar -zxvf star_gdc_index.tar.gz
```
If you need download all the reference files, execute the command shown below.
```shell
sh download_ref.sh
```
The structure of "reference" folder will be like this.
```
├── bed
│   ├── driverGene_refseqGenes.bed
│   ├── housekeeping_refseqGenes.bed
│   └── refseqGenes.bed
├── centrifuge_index
│   ├── p_compressed+h+v.1.cf
│   ├── p_compressed+h+v.2.cf
│   └── p_compressed+h+v.3.cf
├── fasta
│   ├── GRCh38.d1.vd1.fa
│   └── GRCh38.d1.vd1.fa.fai
├── fusion_gdc_index
│   └── GRCh38_v22_CTAT_lib_GDC_Mar162019
├── gencode.v22.annotation.gtf
├── lisa
│   └── hg38
├── msisensor_gdc_index
│   └── hg38
├── pathseq_gdc_index
│   └── hg38
│      ├──pathseq_microbe.fa
│      ├── pathseq_microbe.fa.img 
│      ├── pathseq_microbe.db 
│      ├── GRCh38.d1.vd1.fa.img 
│      └── hg38_k31.hss
├── rsem_gdc_index
│   └── hg38
├── salmon_gdc_index
│   └── gencode.v22.ts.fa
├── star_gdc_index
│   └── hg38
├── trust4
│   ├── bcrtcr.fa
│   └── IMGT.fa
└── vep
    ├── ExAC_nonTCGA.r0.3.1.sites.vep.vcf.gz
    ├── isoform_overrides_uniprot
    ├── vep_gdc_84
    └── VEP_plugins
```
`Note:`

Make sure that your current working directory is "reference".
## 2. Installation
### 2.1 Clone the pipeline repository into your project directory.
```shell
#Command:
git clone https://jinwang0611@bitbucket.org/jinwang0611/rnaseq_pipeline.git
```
### 2.2 Required software
#### 2.2.1 conda
If conda has bas been installed in your system previously, you can skip this step. Otherwise download and configure your conda environment. Follow the prompts on the installer screens, if you are unsure about any setting, accept the defaults. 
```shell
#Command:
wget https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh
bash Miniconda3-latest-Linux-x86_64.sh
conda list  #to see whether you have successfully installed conda
```
#### 2.2.2 Snakemake
Install snakemake in your conda environment. Check the version of your snakemake, make sure > 5.0 to enable some functions working well.
```shell
#Command:
conda install -c bioconda -c conda-forge snakemake
snakemake –version #to see whether you have successfully installed snakemake
```
### 2.3 Optional software
If you have the needs below：
```
1. Estimate microsatellite instability score(msisensor2)
2. Predict epitopes affinity bound to HLA(pvacseq)
3. Infer BCR/TCR sequences(TRUST4)
```
You should install msisensor2,pvacseq and TRUST4 manually, since there are no available conda packages for them.
#### 2.3.1 Msisensor2
There are two options for estimating microsatellite instability score using msisensor, we will run msisensor when you provide tumor-normal paired samples, otherwise we will run msisensor2 when you have tumor samples only. In that case, we integrate msisensor and msisensor2 to enable the mixed samples(some of them are paired, some of them are tumor-only). No conda packages for msisensor2, you need to manually download and execute it.
```shell
#Command:
#install msisensor2 into rnaseq_pipeline directory
git clone https://github.com/niu-lab/msisensor2.git
cd msisensor2
chmod +x msisensor2
```
`Note:`

Make sure that your current working directory is "rnaseq_pipeline"
#### 2.3.2 pvacseq
There are several steps for installing pvacseq manually and configuring the database used in pvacseq
(1) Create an individual environment for pvacseq parallely with your base environment and install pvactools, vcf-annotation-tools
```shell
#Command:
conda create -n pvacseq python=3.7
conda activate pvacseq
pip install pvactools
pip install vcf-annotation-tools
pvacseq  -h  #to see whether you have successfully installed pvacseq
```
(2) Integrate VEP plugins into pvacseq:
```shell
pvacseq install_vep_plugin <VEP_plugin path> 
conda deactivate
```
(3) Install IEDB binding prediction tools. Download the archives for class I and unpack them. Note that running the configure script requires a Python 2 environment. You can create an python2 environment parallelly with your base environment.
```shell
#Command:
conda create -n pvactools_py2.7 python=2.7
conda activate pvactools_py2.7
wget https://downloads.iedb.org/tools/mhci/2.19.2/IEDB_MHC_I-2.19.2.tar.gz
tar -zxvf IEDB_MHC_I-2.19.2.tar.gz
cd mhc_i
./configure
conda deactivate
```
#### 2.3.3 TRUST4
Install TRUST4 into rnaseq_pipeline directory
```shell
#Command:
git clone https://github.com/liulab-dfci/TRUST4.git
```
`Note:`

Make sure that your current working directory is "rnaseq_pipeline"

Finally in your "project" folder, the folder structure will be like this:
```
├── data
│   ├── pair
│   └── single
├── reference
└── rnaseq_pipeline
    ├── ref.yaml
    ├── execution.yaml
    ├── config.yaml
    ├── metasheet.csv
    ├── visulization_reports.py
    ├── rnaseq.snakefile
    ├── snakemake_files
    ├── run_time_memory.snakefile
    ├── envs
    ├── README.md
    ├── src
    ├── msisensor2
    ├── TRUST4
    ├── htmls
    └── static
```

## 3. Configuration
Setup configuration files, including config.yaml, ref.yaml, execution.yaml and metasheet.csv files under rnaseq_pipeline folder and the deseq2 design file under rnaseq_pipeline/static folder.
### 3.1 Metasheet.csv 
You need to prepare a metasheet.csv file containing comma-separated columns. The first column is “SampleName” (the consistent name in your config.yaml file), the second column is “Batch” (fill it if you have otherwise set it empty), the third column is “Tissue” (mark the tissue types for each sample), and you can also define other sample features in the following columns. For example, “Treatment” (Pre-treatment/Post-treatment), Responder (Responder/Non-Responder) and so on.
```
SampleName,PatName,Batch,Tissue,Treatment
R19013725LR01-181589T,run5,1,tumor,Post-treatment
R19013724LR01-181508T,run6,2,tumor,Pre-treatment
R19013718LR01-180289T,run7,2,tumor,Pre-treatment
R19013713LR01-171439T,run8,2,tumor,Post-treatment
R19013702LR01-150521T,run1,1,tumor,Post-treatment
R18074914LR01-180707N,run2,1,normal,Post-treatment
R18074885LR01-171674N,run3,1,normal,Post-treatment
R18074855LR01-150521N,run1,2,normal,Pre-treatment
R18074838LR01-180707T,run2,1,tumor,Pre-treatment
R18074809LR01-171674T,run3,1,tumor,Pre-treatment
R19013727LR01-181874T,run4,2,tumor,Post-treatment
R19013710LR01-170358T,run9,1,tumor,Pre-treatment
```
### 3.2 Config.yaml
Config.yaml file defines the pipeline running parameters, sample information and run settings. There are mainly three types of parameters: (1) sequence information and sample information(use the absolute path of your samples) (2) whether to run specific procedures in certain module or not; (3) parameters of tools.

In the config file, we set salmon and centrifuge as default software instead of rsem and pathseq in quantifying transcripts and microbiota classification respectively. If you want to use rsem or pathseq, just change the relative parameters between true and false.

There are also some parameters ending with phenotypes or phenotype which stands for the sample conditions you want to compare. You should provide these parameters with column names in metasheet.csv as we mentioned before. For those parameters ending with phenotype, you should provide just one column name, while for those parameters ending with phenotypes, you can fill in with multiple column names.

`Note`:

It would be better for you to provide the full path of these files like this.
```
R19013702LR01-150521T:
  - /share/home/liuxl/renpengfei/project_new/data/pair/R19013702LR01-150521T/R19013702LR01-150521T_combined_R1.fastq.gz
  - /share/home/liuxl/renpengfei/project_new/data/pair/R19013702LR01-150521T/R19013702LR01-150521T_combined_R2.fastq.gz
R18074855LR01-150521N:
  - /share/home/liuxl/renpengfei/project_new/data/pair/R18074855LR01-150521N/R18074855LR01-150521N_combined_R1.fastq.gz
  - /share/home/liuxl/renpengfei/project_new/data/pair/R18074855LR01-150521N/R18074855LR01-150521N_combined_R2.fastq.gz 
```

For each run, it contains a pair of samples or only a tumor samples. (please provide Tumor sample first and then Normal sample if you have). You can set the patient name as the name of each run name.
```
run1:
  - R19013702LR01-150521T
  - R18074855LR01-150521N
run2:
  - R18074838LR01-180707T
  - R18074914LR01-180707N
```
### 3.3	Ref.yaml 
Ref.yaml file define the index required by running pipeline. Please make sure that you have prepared the reference files already and change the correct path. 

As we mentioned before, we set salmon and centrifuge as default instead of rsem and pathseq. So in the default situation, you just need to provide the reference files of salmon and centrifuge and ignore the reference of rsem and pathseq.

### 3.4 Execution.yaml 
We set the pipeline with feasible options. For example, the users already have tpm expression data and only want to run some of modules. In this case, they can change the settings in Execution.yaml file which provides the settings of running certain module or not (true represents for running the tool while false represents not).
### 3.5 Deseq2 design file
Finally, you still need to provide the tab-separated design file(s) containing three columns: SampleName, Batch, Condition. The SampleName and Batch columns are the same to the contents in metasheet.csv. The condition column is set up for declaring the group you want to compare in the differential gene analysis. Remember to put it under static/deseq2 folder. 
e.g: If the users want to compare groups in "Tissue" column, then they should create a tab-separated file named with "Tissue" and put it under static/deseq2. In that case, you will find it as static/deseq2/Tissue.
## 4 Execution
#### Step1: Check the pipeline by dry run to make sure the correct script and data usage.
```
snakemake -s rnaseq.snakefile -n 
```
This command gives a series of steps that will be performed. If everything is good, then proceed further.
#### Step2: Run the pipeline
```
nohup snakemake -s rnaseq.snakefile --use-conda -j 16 &
```
Here, -j set the cores for parallelly run, --use-conda set snakemake install the separated tool environments automatically for the first time. Detailed usage of each tool is explained below. More resources and the instructions about snakemake can be found [here](https://snakemake.readthedocs.io/en/stable/).

`Note:`
If one user has created the environment already in a specific folder (e.g. /path/existing/.snakemake/conda), the other users also can share with the existing environments by specifying the conda environment path with “ --conda-prefix /path/existing/.snakemake/conda”

#### Step3: Check the time and memory consumption 
Execute the command as shown below, you will get a line chart representing the time and memory usage. Remember the run_time_memory.snakefile is under rnaseq_pipeline folder and the chart is under images/consumption.
```
snakemake -s run_time_memory.snakefile --use-conda
```
## 5 Visualization
Execution of the command as shown below can generate html visualization files which contain key results of each module. Remember that execute the command under rnaseq_pipeline directory and those html files are under reports folder. When you want to visualize the reports in local or remote, put the three folders "files/","images/","reports/" together all the time. This is an visualization demo by clicking [here](http://cistrome.org/~jinwang/kraken_pipeline_test_0107/reports/rnaseq_Meta.html)
```shell
mkdir reports
python visulization_reports.py
```










