#!/usr/bin/env python

#-------------------------------DESeq2 for differential gene expression-----------------------#
# @author: Jin Wang; @ref: VIPER
# @email: jwang0611@gmail.com

import pandas as pd
metadata = pd.read_csv(config["metasheet"], index_col=0, sep=',')
data_type = "rsem" if config["rsem"] else "salmon"


def getTreatAll(phenotypes):
    treats_list = {}
    for phenotype in phenotypes:
        treats = [i for i in list(set(metadata[phenotype].tolist()))]
        treats_list[phenotype] = treats
    return treats_list


def wgcna_targets(wildcards):
    ls = []
    ls.append("images/wgcna/soft_thresholding_power_plot.png")
    ls.append("images/wgcna/block_module_plot.png")
    ls.append("images/wgcna/co-expression_network_plot.png")
    for phenotype in config["wgcna_clinical_phenotypes"]:
        if len(set(metadata[phenotype].tolist()))>1:
            ls.append("images/wgcna/%s_module_traits_plot.png" % phenotype)
        # for treat in list(set(metadata[phenotype].tolist())):
        #     ls.append("analysis/wgcna/%s_%s_Module_Genes_plot.png" % (phenotype, treat))
        #     ls.append("analysis/deseq2/%s_%s_Connectivity_Genes_plot.png" % (phenotype, treat))
    return ls

rule wgcna_all:
    input:
        wgcna_targets


rule wgcna_module_triats:
    input:
        expression = "analysis/combat/tpm_convertID.batch"
    output:
        soft_thresholding_power = "images/wgcna/soft_thresholding_power_plot.png",
        module_blocks = "images/wgcna/block_module_plot.png",
        network = "images/wgcna/co-expression_network_plot.png",
        module_trait = expand("images/wgcna/{phenotype}_module_traits_plot.png", phenotype=config["wgcna_clinical_phenotypes"])
        # module_hub = dynamic("analysis/wgcna/{phenotype}}_{treat}_Connectivity_Genes_plot.png")
        # lambda wildcards: expand("analysis/wgcna/{{phenotype}}_{treat}_Connectivity_Genes_plot.png", treat=getTreatAll(config["wgcna_clinical_phenotypes"])[wildcards.phenotype])
    log:
        "logs/wgcna/wgcna.log"
    params:
        triats = config["metasheet"],
        phenotypes = lambda wildcards: ','.join(str(i) for i in config["wgcna_clinical_phenotypes"]),
        top_n = config["wgcna_top_n_genes"],
        minModuleSize = config["wgcna_minModuleSize"],
        cytoscape_path = "analysis/wgcna/",
	nthreads = config["wgcna_threads"],
        out_path = "images/wgcna/"
    message: 
        "Running WGCNA on the samples"
    benchmark:
        "benchmarks/wgcna/WGCNA.benchmark"
    conda: "../envs/stat_perl_r.yml"
    shell:
	#"mkdir -p {params.cytoscape_path}"
        "Rscript src/wgcna_plot.R -e {input.expression} -a {params.nthreads} -t {params.triats} -c {params.phenotypes} -n {params.top_n} -m {params.minModuleSize} -o {params.out_path}"
	    " && mkdir -p {params.cytoscape_path}"
        " && rm ./WGCNA_TOM-block.1.RData "
        " && touch ./CytoscapeInput-edges* "
        " && touch ./CytoscapeInput-nodes* "
        " && mv ./CytoscapeInput-edges* {params.cytoscape_path} "
        " && mv ./CytoscapeInput-nodes* {params.cytoscape_path} "
        # " && mkdir -p files/wgcna"
        # " && mv images/wgcna/*txt files/wgcna"

