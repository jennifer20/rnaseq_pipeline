#!/usr/bin/env python

#-------------------------------VARIANT ANNOTATION-----------------------#
# @author: Jin Wang
# @email: jwang0611@gmail.com

def ssgsea_targets(wildcards):
    ls = []
    ls.append("files/ssgsea/ssgsea_all_terms_score.txt")
    for comparison in config['ssgsea_comparisons']:
        ls.append("images/ssgsea/%s/%s_ssgsea_plot.png" % (comparison, comparison))
    return ls

rule ssgsea_all:
    input:
        ssgsea_targets

rule ssgsea_score:
    input:
        "analysis/combat/tpm_convertID.batch"
    output:
        score = "files/ssgsea/ssgsea_all_terms_score.txt",
        plots = expand("images/ssgsea/{comparison}/{comparison}_ssgsea_plot.png", comparison=config['ssgsea_comparisons'])
    log:
        "logs/ssgsea/ssgsea.log"
    params:
        gmt = config['gmt_path'],
        comparison = lambda wildcards: ','.join(str(i) for i in list(config['ssgsea_comparisons'])),
        meta = config['metasheet'],
        top_n = config['ssgsea_top_n_terms'],
        outpath = "images/ssgsea/",
        path="set +eu;source activate %s" % config['stat_root'],
    message:
        "Running single sample gene set enrichment analysis" 
    benchmark:
        "benchmarks/ssgsea/ssgsea.benchmark"
    conda: "../envs/stat_perl_r.yml"
    shell:
        "{params.path}; Rscript src/ssgsea_plot.R -e {input} -g {params.gmt} -c {params.comparison} -m {params.meta} -n {params.top_n} -o {params.outpath}"
        " && mv images/ssgsea/ssgsea_all_terms_score.txt files/ssgsea/ssgsea_all_terms_score.txt"
