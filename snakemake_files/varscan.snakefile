#!/usr/bin/env python

#-------------------------------SOMATIC VARIATIONS----------------------------#
# @author: Jin Wang; @ref: VIPER
# @email: jwang0611@gmail.com


def runsVarscanHelper(wildcards):
    r = config['runs'][wildcards.run]    
    samples=[]
    if config['driver_extract']:
        samples=["analysis/star/{sample}/{sample}.sorted.driver.extract.bam".format(sample=sample) for sample in r]   
    else:         
        samples=["analysis/star/{sample}/{sample}.sorted.bam".format(sample=sample) for sample in r]
    return samples
 

def varscan_targets(wildcards):
    ls=[]
    for run in config["runs"]:  
        if len((config["runs"][run])) >= 2:
            samples=config["runs"][run]
            ls.append("analysis/varscan/pair/%s/%s.varscan.somatic.base.snp.vcf" % (run, run))
            ls.append("analysis/varscan/pair/%s/%s.varscan.somatic.base.indel.vcf" % (run, run))
            ls.append("analysis/varscan/pair/%s/%s.varscan.somatic.base.snp.Somatic.hc.vcf" % (run, run))
            ls.append("analysis/varscan/pair/%s/%s.varscan.somatic.base.indel.Somatic.hc.vcf" % (run, run))
            ls.append("analysis/varscan/pair/%s/%s.varscan.somatic.base.snp.Somatic.hc.filter.vcf" % (run, run))
            if config['driver_extract']:
                ls.append("analysis/star/%s/%s.sorted.driver.extract.bam" % (samples[0], samples[0]))  
                ls.append("analysis/star/%s/%s.sorted.driver.extract.bam" % (samples[1], samples[1]))
    return ls

rule varscan_all:
    input:
        varscan_targets

rule driver_extract:
    input:
        "analysis/star/{sample}/{sample}.sorted.bam"  
    output:
        "analysis/star/{sample}/{sample}.sorted.driver.extract.bam" 
    log:
        "logs/star/{sample}.driver_extract.log"      
    params:
        driver_bed = config["driver_bed_path"]
    benchmark:
        "benchmarks/star/{sample}.sorted.driver_extract.benchmark"
    conda: "../envs/rseqc_env.yml"
    shell:
        "bedtools intersect -a {input} -b {params.driver_bed} > {output}"

rule varscan_call_somatic:
    input:
        fa_file = config["fasta_path"], 
        pair = runsVarscanHelper
    output:
        "analysis/varscan/pair/{run}/{run}.varscan.somatic.base.snp.vcf",
        "analysis/varscan/pair/{run}/{run}.varscan.somatic.base.indel.vcf"
    log:
        "logs/varscan/pair/{run}.varscan_somatic.log"
    params:
        min_coverage = config["varscan_min_converage"],
        min_var_freq = config["varscan_min_var_freq"],
        somatic_p = config["varscan_somatic_pval"],
        normal_purity = config["varscan_normal_purity"],
        tumor_purity = config["varscan_tumor_purity"],
        prefix = "analysis/varscan/pair/{run}/{run}.varscan.somatic.base"
    message: 
        "Running VarScan on {wildcards.run}"
    benchmark:
        "benchmarks/varscan/pair/{run}.varscan_somatic.benchmark"
    conda: "../envs/varscan_env.yml"
    shell:
        "samtools mpileup -f {input.fa_file} -q 1 -B {input.pair} | "
        "varscan somatic --output-file {params.prefix} --output-vcf 1 --mpileup 1 "
        "--min-coverage {params.min_coverage} --min-var-freq {params.min_var_freq} --somatic-p-value {params.somatic_p} "
        "--normal_purity {params.normal_purity} --tumor_purity {params.tumor_purity}"
        
rule varscan_filter_somatic:
    input:
        snp_vcf="analysis/varscan/pair/{run}/{run}.varscan.somatic.base.snp.vcf",
        indel_vcf="analysis/varscan/pair/{run}/{run}.varscan.somatic.base.indel.vcf"
    output:
        snp_hc_vcf = "analysis/varscan/pair/{run}/{run}.varscan.somatic.base.snp.Somatic.hc.vcf",
        indel_hc_vcf = "analysis/varscan/pair/{run}/{run}.varscan.somatic.base.indel.Somatic.hc.vcf",
        snp_hc_filter_vcf = "analysis/varscan/pair/{run}/{run}.varscan.somatic.base.snp.Somatic.hc.filter.vcf"
    log:
        "logs/varscan/pair/{run}.varscan_somatic_filter.log"
    message: 
        "Running Varscan filtering on {wildcards.run}"
    benchmark:
        "benchmarks/varscan/pair/{run}.varscan_somatic_filter.benchmark"
    conda: "../envs/varscan_env.yml"
    shell:
        "varscan processSomatic {input.snp_vcf}"
        " && varscan processSomatic {input.indel_vcf}"
        " && varscan somaticFilter {output.snp_hc_vcf} --indel_file {output.indel_hc_vcf} --output-file {output.snp_hc_filter_vcf}"

