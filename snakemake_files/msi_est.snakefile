#!/usr/bin/env python

#-------------------------------
# @author: Jin Wang; @ref: VIPER
# @email: jwang0611@gmail.com

normalization = "" if config['control'] else "--normalize"

def msi_targets(wildcards):
    ls = []
    ls.append("analysis/msi_est/tpm_convertID_batch_msi_score")
    return ls

rule msi_all:
    input:
        msi_targets


rule msi_score:
    input:
        "analysis/tide/tpm_convertID_batch_Entrez_normalize_control.txt"
    output:
        directory("analysis/msi_est/tpm_convertID_batch_msi_score")
    message: 
        "Running MSI_est"
    benchmark:"benchmarks/msi_est/msi_est_score.benchmark"
    params: 
        run_normalization = normalization
    shell:
        "python static/msi_est/main.py -e {input} -o {output} " 
