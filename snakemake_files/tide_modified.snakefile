#!/usr/bin/env python

#-------------------------------
# @author: Jin Wang; @ref: VIPER
# @email: jwang0611@gmail.com

normalization = "" if config['control'] else "--normalize"

def tide_targets(wildcards):
    ls = []
    ls.append("analysis/tide/tpm_convertID_batch_Entrez.txt")
    ls.append("analysis/tide/tpm_convertID_batch_tide_score.txt")
    ls.append("images/tide/tide_user_tcga.png")
    ls.append("images/tide/tide_score.png")
    if config['control']:
        ls.append("analysis/tide/tpm_convertID_batch_Entrez_normalize_control.txt")
    return ls

rule tide_all:
    input:
        tide_targets

rule entrez_convert:
    input:
        "analysis/combat/tpm_convertID.batch"
    output:
        "analysis/tide/tpm_convertID_batch_Entrez.txt"
    message: 
        "Running ID entrez_convert"
    benchmark:
        "benchmarks/tide/entrez_convert.benchmark"
    params: 
        prefix = "analysis/tide/tpm_convertID_batch_Entrez.txt",
        tool_type = "tide"
    shell:
        "python src/ConvertID.py -m {input} -t {params.tool_type} -r static/msi_est/name.map -p {params.prefix}"

rule tumor_expr_normalization:
    input:
        "analysis/tide/tpm_convertID_batch_Entrez.txt"
    output:
        "analysis/tide/tpm_convertID_batch_Entrez_normalize_control.txt"
    message: 
        "Running expr normalization with entrez converted ID"
    params: 
        meta_info = config["metasheet"],
        batch = config["batch"],
        prefix = "analysis/tide/tpm_convertID_batch_Entrez"
    shell:
        "python src/TIDE_Input_Normalization.py -i {input} -m {params.meta_info} -p {params.prefix}"

rule tide_score:
    input:
        "analysis/tide/tpm_convertID_batch_Entrez_normalize_control.txt"
    output:
        "analysis/tide/tpm_convertID_batch_tide_score.txt"
    message: 
        "Running TIDE"
    benchmark:
        "benchmarks/tide/tide_score.benchmark"
    log:
        "logs/tide/tide_score.log"
    params: 
        meta_info = config["metasheet"],
        outdir = "analysis/tide/tpm_convertID_batch_tide_score.txt",
        cancer = config["tide_cancer"],
        run_normalization = normalization
    # conda: "../envs/py3_env.yml"
    shell:
        "python static/tide/main.py -e {input} -o {params.outdir} -c {params.cancer} {params.run_normalization}"

rule tide_plot:
    input:
        "analysis/tide/tpm_convertID_batch_tide_score.txt"
    output:
        "images/tide/tide_user_tcga.png",
        "images/tide/tide_score.png"
    message:
        "plot on tide score"
    benchmark:
        "benchmarks/tide/tide_plot.benchmark"
    log:
        "logs/tide/tide_plot.log"
    params:
        cancer = config["cancer_type"],
        outpath = "images/tide/",
        path="set +eu; source activate %s" % config['stat_root']
    conda: "../envs/stat_perl_r.yml"
    shell:
        """{params.path}; Rscript src/tide_plot.R --input {input} --cc {params.cancer} --outdir {params.outpath}"""
    

