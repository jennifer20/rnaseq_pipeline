#!/usr/bin/env python

#-------------------------------
# @author: Jin Wang; @ref: VIPER
# @email: jwang0611@gmail.com


def optitype_targets(wildcards):
    """Generates the targets for this module"""
    ls = []
    for sample in config["samples"]:
        ls.append("analysis/optitype/%s/%s_result.tsv" % (sample,sample))
        ls.append("analysis/optitype/%s/%s_coverage_plot.pdf" % (sample,sample))
    ls.append("files/optitype/merged_HLAI_typing.txt")
    ls.append("images/optitype/hla_typing_frequency_plot.png")
    return ls

rule optitype_all:
    input:
        optitype_targets

def getFastqHLA(wildcards):
    return config["samples"][wildcards.sample]

rule optitype:
    input:
        getFastqHLA
        # mates = expand("data/{{sample}}/{{sample}}_{mate}.fastq.gz", mate=config["mate"])
    output:
        results = "analysis/optitype/{sample}/{sample}_result.tsv",
        coverage = "analysis/optitype/{sample}/{sample}_coverage_plot.pdf"
    message: "Running OptiType on {wildcards.sample}"
    benchmark:
        "benchmarks/optitype/{sample}.optitype.benchmark"
    log:
        "logs/optitype/{sample}.optitype.log"
    params:
        # pypath = config["python2_path"],
        # optitype_path = config["optitype_path"],
        outpath = "analysis/optitype/{sample}",
        cfg_path = config["cfg_init"],
        prefix = "{sample}"
        path="source activate %s" % config['optitype_root'],
        optitype_config="static/optitype/config.ini",
    conda: "../envs/optitype_env.yml"
    shell:
        "OptiTypePipeline.py -i {input} --rna -v -o {params.outpath} -c {params.cfg_path} -p {params.prefix}"
        " && rm -f {params.outpath}/*bam"

rule hla_plot:
    input:
        expand("analysis/optitype/{sample}/{sample}_result.tsv", sample=config['samples'])
    output:
        merged_hla = "files/optitype/merged_HLAI_typing.txt",
        png = "images/optitype/hla_typing_frequency_plot.png"
    log:
        "logs/optitype/immune_hla.log"
    message:
        "Running HLA typing frequency summary"
    benchmark:
        "benchmarks/optitype/hla_plot.benchmark.txt"
    params:
        outpath = "images/optitype/"
    #conda: "../envs/stat_perl_r.yml"
    shell:
        """cat {input} | sed '1 !{{/Reads/d;}}' | cut -f2-10 > tmp """
        """ && awk '{{print FILENAME}}' {input}| awk -F '/' '{{print $3}}' | sort | uniq | awk 'BEGIN{{print "sample"}}{{print $0}}' | paste - tmp > {output.merged_hla} """ ###add sample name as one column in fusion result
        """ && rm tmp """
        """ && Rscript src/hla_plot.R --hla {output.merged_hla} --outdir {params.outpath}"""
       
