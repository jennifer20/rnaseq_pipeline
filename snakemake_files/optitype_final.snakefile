#!/usr/bin/env python

#-------------------------------
# @author: Aashna Jhaveri; @ref: VIPER
# @email: jwang0611@gmail.com
_optitype_threads=16

def optitype_targets(wildcards):
    """Generates the targets for this module"""
    ls = []
    for sample in config["samples"]:
        ls.append("analysis/optitype/%s/%s_result.tsv" % (sample,sample))
        ls.append("analysis/optitype/%s/%s_coverage_plot.pdf" % (sample,sample))
        ls.append("analysis/optitype/%s/%s.sorted.chr6.bam" % (sample,sample))
        ls.append("analysis/optitype/%s/%s.sorted.chr6.end1.fastq" % (sample,sample))
        ls.append("analysis/optitype/%s/%s.sorted.chr6.end2.fastq" % (sample,sample))
    ls.append("analysis/optitype/merged_HLAI_typing.txt")
    ls.append("images/optitype/hla_typing_frequency_plot.png")
    return ls

rule optitype_all:
    input:
        optitype_targets

def getFastqHLA(wildcards):
    return config["samples"][wildcards.sample]


rule optitype_extract_chr6:
    """Extract chr6 by sambamba"""
    input:
      in_sortbamfile = "analysis/star/{sample}/{sample}.sorted.bam"
    output:
      chr6sortbamfile = "analysis/optitype/{sample}/{sample}.sorted.chr6.bam"
    threads: _optitype_threads
    #conda: "../envs/optitype_env.yml"
    benchmark:
     "benchmarks/optitype/{sample}/{sample}.optitype_extract_chr6.txt"
    shell:
     """sambamba view -t {threads} -f bam -h {input.in_sortbamfile}  chr6 > {output.chr6sortbamfile}"""

rule optitype_bamtofastq:
    """Convert the sorted.chr6.bam file to fastq by samtools"""
    input:
      in_sortchr6bamfile = "analysis/optitype/{sample}/{sample}.sorted.chr6.bam"
    output:
      chr6fastqfile1 = "analysis/optitype/{sample}/{sample}.sorted.chr6.end1.fastq",
      chr6fastqfile2 = "analysis/optitype/{sample}/{sample}.sorted.chr6.end2.fastq"
    conda: "../envs/optitype_env.yml"
    log: "logs/optitype/{sample}/{sample}.optitype_bamtofastq.log"
    benchmark:
      "benchmarks/optitype/{sample}/{sample}.optitype_bamtofastq.txt"
    shell:
      """samtools fastq -@ 2 -1 {output.chr6fastqfile1} -2 {output.chr6fastqfile2} {input.in_sortchr6bamfile} 2> {log}"""

rule optitype_hlatyping:
    input:
        #getFastqHLA
        file1 = "analysis/optitype/{sample}/{sample}.sorted.chr6.end1.fastq",
        file2 = "analysis/optitype/{sample}/{sample}.sorted.chr6.end2.fastq"
        # mates = expand("data/{{sample}}/{{sample}}_{mate}.fastq.gz", mate=config["mate"])
    output:
        results = "analysis/optitype/{sample}/{sample}_result.tsv",
        coverage = "analysis/optitype/{sample}/{sample}_coverage_plot.pdf"
    message: "Running OptiType on {wildcards.sample}"
    benchmark:
        "benchmarks/optitype/{sample}.optitype.benchmark"
    log:
        "logs/optitype/{sample}.optitype.log"
    params:
     #   # pypath = config["python2_path"],
     #   # optitype_path = config["optitype_path"],
        outpath = "analysis/optitype/{sample}",
        cfg_path = config["cfg_init"],
        prefix = "{sample}"
    conda: "../envs/optitype_env.yml"
    shell:
        "OptiTypePipeline.py -i {input.file1} {input.file2} --rna -v -o {params.outpath} -c {params.cfg_path} -p {params.prefix}"



##Create a common matrix file from all the samples
rule merged_hla__matrix:
    input:
       expand("analysis/optitype/{sample}/{sample}_result.tsv" , sample=config['samples'])
    output:
       matrix_all="analysis/optitype/merged_HLAI_typing.txt",
       png = "images/optitype/hla_typing_frequency_plot.png"
    params:
       outpath = "images/optitype/"
    log:
       "logs/optitype/matrix_hla.log"
    message:
       "Creating matrix for all the samples into one"
    benchmark:
       "benchmarks/optitype/hla_matrix.benchmark.txt"
    conda:
       "../envs/stat_perl_r.yml"
    shell:
       """cat {input} | sed '1 !{{/Reads/d;}}' | cut -f2-10 > tmp """
       """ && awk '{{print FILENAME}}' {input}| awk -F '/' '{{print $3}}' | sort | uniq | awk 'BEGIN{{print "sample"}}{{print $0}}' | paste - tmp > {output.matrix_all} """
       """ && Rscript src/hla_plot.R --hla {output.matrix_all} --outdir {params.outpath}"""

