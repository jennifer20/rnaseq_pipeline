#!/usr/bin/env python

#-------------------------------MSISENSOR-------------------------
# @author: Jin Wang; @ref: VIPER
# @email: jwang0611@gmail.com

def runsMSisensorHelper(wildcards):
    r = config['runs'][wildcards.run]    
    bam=["analysis/star/{sample}/{sample}.sorted.bam".format(sample=sample) for sample in r]
    index=["analysis/star/{sample}/{sample}.sorted.bam.bai".format(sample=sample) for sample in r]
    samples=bam+index
    return samples
    
def getPair(wildcards):  
        Pair=runsMSisensorHelper(wildcards)
        if len(Pair)>=4:
            return Pair

def getSingle(wildcards):
        Single=runsMSisensorHelper(wildcards)
        if len(Single)<4:
            return Single

def msisensor_input(wildcards):
    ls=[]
    for run in config["runs"]: 
        if len((config["runs"][run])) >= 2:
            ls.append("analysis/msisensor/pair/%s/%s_msisensor" % (run, run))
        else:
            ls.append("analysis/msisensor/single/%s/%s_msisensor" % (run, run))
    return ls

def get_runs_tumor(wildcards):
    ls = []
    for run in config["runs"]:
        ls.append(config["runs"][run][0])
    return ls

def msisensor_targets(wildcards):
    ls=[]
    for run in config["runs"]: 
        if len((config["runs"][run])) >= 2:
            ls.append("analysis/msisensor/pair/%s/%s_msisensor" % (run, run))
            ls.append("analysis/msisensor/pair/%s/%s_msisensor_dis" % (run, run))
            ls.append("analysis/msisensor/pair/%s/%s_msisensor_germline" % (run, run))
            ls.append("analysis/msisensor/pair/%s/%s_msisensor_somatic" % (run, run))
        else:
            ls.append("analysis/msisensor/single/%s/%s_msisensor" % (run, run))
            ls.append("analysis/msisensor/single/%s/%s_msisensor_dis" % (run, run))
            ls.append("analysis/msisensor/single/%s/%s_msisensor_somatic" % (run, run))
    ls.append("files/msisensor/msi_score.txt")
    ls.append("images/msisensor/msi_score_density_plot.png")
    ls.append("images/msisensor/msi_score_comparison.png")
    return ls



rule msisensor_all:
    input:
        msisensor_targets


rule msisensor_pair_call:
    input:
        getPair
    output:
        "analysis/msisensor/pair/{run}/{run}_msisensor",
        "analysis/msisensor/pair/{run}/{run}_msisensor_dis",
        "analysis/msisensor/pair/{run}/{run}_msisensor_germline",
        "analysis/msisensor/pair/{run}/{run}_msisensor_somatic"
    message: 
        "Running msisensor on {wildcards.run}"
    benchmark:
        "benchmarks/msisensor/pair/{run}.msisensor.benchmark"
    log:
        "logs/msisensor/pair/{run}.msisensor.log"
    conda: "../envs/msisensor_env.yml"
    params:
        msi_ref = config["msisensor_index"],
        prefix = "analysis/msisensor/pair/{run}/{run}_msisensor"
    shell:
        "msisensor msi -d {params.msi_ref} -n {input[1]} -t {input[0]} -o {params.prefix}"


rule msisensor_tumor_call:
    input:
        getSingle
    output:
        "analysis/msisensor/single/{run}/{run}_msisensor",
        "analysis/msisensor/single/{run}/{run}_msisensor_dis",
        "analysis/msisensor/single/{run}/{run}_msisensor_somatic"
    message: 
        "Running msisensor on {wildcards.run}"
    benchmark:
        "benchmarks/msisensor/single/{run}.msisensor.benchmark"
    log:
        "logs/msisensor/single/{run}.msisensor.log"
    params:
        prefix = "analysis/msisensor/single/{run}/{run}_msisensor"
    shell:
        "msisensor2/msisensor2 msi -M msisensor2/models_hg38 -t {input[0]} -o {params.prefix}"


rule msisensor_plot:
    input:
        msisensor_input
    output:
        msi_score = "files/msisensor/msi_score.txt",
        msi_density = "images/msisensor/msi_score_density_plot.png",
        msi_comparison = "images/msisensor/msi_score_comparison.png"
    log:
        "analysis/variant/variant_missensor.log"
    message: 
        "Running msisensor ploting"
    benchmark:
        "analysis/variant/msisensor_plot.benchmark.txt"
    conda: "../envs/stat_perl_r.yml"
    params:
        outpath = "images/msisensor/",
        run = get_runs_tumor,
        phenotype = lambda wildcards: ','.join(str(i) for i in config["msi_clinical_phenotypes"]),
        meta = config["metasheet"]
    shell:  
        """cat {input} | sed '/Total_Number_of_Sites/d' | awk -F '\t' '{{print $3}}' > tmp """ 
        """&&  echo {params.run}| xargs -n1 | paste - tmp > {output.msi_score}"""
        """&& rm tmp """
        """&& Rscript src/msi_plot.R --msiscore {output.msi_score}  --outdir {params.outpath} --phenotype {params.phenotype} --meta {params.meta}"""
