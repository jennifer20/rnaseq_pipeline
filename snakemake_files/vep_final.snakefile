#!/usr/bin/env python

#-------------------------------VARIANT ANNOTATION-----------------------#
# @author: Jin Wang
# @email: jwang0611@gmail.com
import os
import subprocess


def runsHelperGetSample(wildcards, iindex):
    #Given a snakemake wildcards, an iindex - 0 for Normal, 1 for Tumor,and a Python format string (ref: https://www.programiz.com/python-programming/methods/string/format) returns the template string with the run name"""
    tmp = []
    r = config['runs'][wildcards.run]
    print(r)

    #check that we have a valid pair
    if len(r) >=2:
        sample_name = r[iindex]
        tmp.append(sample_name)
    else:
        #NOTE: I can't figure out a proper kill command so I'll do this
        tmp=["ERROR! BAD pairing for run--requires at least two samples: %s" % (wildcards.run)]
    print(tmp)
    return tmp

def getNormal(wildcards):
    return runsHelperGetSample(wildcards, 1)

def getTumor(wildcards):
    return runsHelperGetSample(wildcards, 0)

def somatic_input(wildcards):
    ls=[]
    for run in config["runs"]: 
        if len((config["runs"][run])) >= 2:
            ls.append("analysis/vep/pair/%s/%s.varscan.somatic.base.snp.Somatic.hc.filter.maf" % (run, run))
    print(ls)
    return ls

def vep_targets(wildcards):
    ls = []
    pair_count = 0
    for run in config["runs"]:
        if len((config["runs"][run])) >= 2:
            pair_count += 1
            ls.append("analysis/vep/pair/%s/%s.varscan.somatic.base.snp.Somatic.hc.filter.maf" % (run, run))
            ls.append("images/variant/individual/%s_maftools_summary_plot.png" % (run))
    if pair_count >= 1:
        ls.append("images/variant/cohort/maftools_table.txt")
        ls.append("analysis/vep/merged_processed_dbSNP.maf")
        ls.append("images/variant/cohort/maftools_summary_plot.png")
        ls.append("images/variant/cohort/maftools_titv_plot.png")
        ls.append("images/variant/cohort/maftools_onco_plot.png")
        ls.append("images/variant/cohort/maftools_lollipop_plot.pdf")
        ls.append("images/variant/cohort/maftools_tcga_plot.png")
        ls.append("images/variant/cohort/maftools_drug_plot.png")
    return ls


rule vep_all:
    input:
        vep_targets

rule vep_snp_annotation:
    input:
        vcf = "analysis/varscan/pair/{run}/{run}.varscan.somatic.base.snp.Somatic.hc.filter.vcf",
    output:
        maf = "analysis/vep/pair/{run}/{run}.varscan.somatic.base.snp.Somatic.hc.filter.maf"
    log:
        "logs/vep/pair/{run}.vep_snp_annot.log"
    params:
        # vcf2maf_path = config["vcf2maf_path"],
        vep_custom_enst = config["vep_custom_enst"],
        fa_path = config["fasta_path"],
        germinal_var_path = config["vep_germinal_var"],
        normal_id = getNormal,
        tumor_id = getTumor,
        vep_data = config["vep_data"],
        cache_version = config['cache_version'],
        path="source activate %s" % config['vep_root'],
    message: 
        "Running VEP"
    benchmark:
        "benchmarks/vep/pair/{run}.vep_snp_annot.benchmark"
    conda: "../envs/vep_env.yml"
    shell:
        #"""vep_path=$(python -c "import os; import subprocess; print(os.path.join(subprocess.check_output('echo $CONDA_PREFIX',shell=True).decode('utf-8').strip(), 'bin'))") && """
        #"""sed -i "s/--af_gnomad//g" $vep_path/vc#f2maf.pl && """
        """{params.path}; vcf2maf.pl """
        """--input-vcf {input.vcf} """
        """--output-maf {output.maf} """ 
        """--custom-enst {params.vep_custom_enst} """
        """--ref-fasta {params.fa_path} """
        """--tumor-id {params.tumor_id} """
        """--normal-id {params.normal_id} """
        """--vcf-tumor-id TUMOR """
        """--vcf-normal-id NORMAL """
        """--species homo_sapiens """ #mus_musculus for mouse
        """--ncbi-build GRCh38 """
        """--filter-vcf {params.germinal_var_path} """
        """--cache-version {params.cache_version} """
        """--vep-path {params.path} """
        """--vep-data {params.vep_data}"""
        
rule somatic_cohort_plot:
    input:
        somatic_input
    output:
        merged_maf = "analysis/vep/merged_processed_dbSNP.maf",
        html_maf_table = "images/variant/cohort/maftools_table.txt",
        summary_plot = "images/variant/cohort/maftools_summary_plot.png",
        titv_plot = "images/variant/cohort/maftools_titv_plot.png",
        onco_plot = "images/variant/cohort/maftools_onco_plot.png",
        lillipop_plot = "images/variant/cohort/maftools_lollipop_plot.pdf",
        tcga_plot = "images/variant/cohort/maftools_tcga_plot.png",
        drug_plot = "images/variant/cohort/maftools_drug_plot.png"
    log:
        "logs/variant/somatic_plot.log"
    message: 
        "Running maf processing on the samples"
    benchmark:
        "benchmarks/variant/somatic_plot.benchmark"
    params:
        top_n = config['maftool_top_n'],
        n_pattern = config['maftool_n_pattern'],
        cohort_dir = "images/variant/cohort/",
        meta = config["metasheet"],
        path="set +eu;source activate %s" % config['stat_root'],
    conda: "../envs/stat_perl_r.yml"
    shell:
        """cat {input} | sed '/#/d' | sed '2,${{/Hugo_Symbol/d}}' > {output.merged_maf} """
        """ && {params.path}; Rscript src/maftool_plot.R --maf {output.merged_maf} --top_n {params.top_n} --n_pattern {params.n_pattern} --cohort_outdir {params.cohort_dir} --metasheet {params.meta}"""

rule somatic_individual_plot:
    input:
        "analysis/vep/pair/{run}/{run}.varscan.somatic.base.snp.Somatic.hc.filter.maf" 
    output:
        "images/variant/individual/{run}_maftools_summary_plot.png"
    message: 
        "Running maf processing on the single samples"
    params:
        ss_dir = "images/variant/individual/{run}",
        path="set +eu;source activate %s" % config['stat_root'],
    conda: "../envs/stat_perl_r.yml"
    shell:
        """{params.path}; Rscript src/maftool_single_plot.R --maf {input} --outdir {params.ss_dir}"""

