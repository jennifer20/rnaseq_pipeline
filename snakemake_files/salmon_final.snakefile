#!/usr/bin/env python

#-------------------------------RSEM gene quantification------------------------#
# @author: Jin Wang @ref:VIPER
# @email: jwang0611@gmail.com
# @date: May, 2019
_salmon_threads = 8
import pandas as pd

metadata = pd.read_csv(config["metasheet"], index_col=0, sep=',')

def merge_sep_inputs(inputs):
    inputs_format = ' -f '.join(str(i) for i in list(inputs)[0])
    return inputs_format   

def salmon_targets(wildcards):
    ls = []
    if config['rsem']:
        for sample in config["samples"]:
            ls.append("analysis/rsem/%s/%s.isoforms.results" % (sample, sample))
            ls.append("analysis/rsem/%s/%s.genes.results" % (sample, sample))
        ls.append("analysis/rsem/rsem_tpm_iso_matrix.csv")
        ls.append("analysis/rsem/rsem_tpm_gene_matrix.csv")
    else:
        for sample in config["samples"]:
            ls.append("analysis/salmon/%s/%s.quant.sf" % (sample, sample))
        ls.append("analysis/salmon/salmon_tpm_matrix.csv")
    return ls


rule salmon_all:
    input:
        salmon_targets


rule rsem_quantification:
    input:
        "analysis/star/{sample}/{sample}.transcriptome.bam" #just to make sure STAR output is available before STAR_Fusion
    output:
        rsem_transcript_out = "analysis/rsem/{sample}/{sample}.isoforms.results",
        rsem_gene_out = "analysis/rsem/{sample}/{sample}.genes.results"
    log:
        "logs/rsem/{sample}.rsem.log"
    message: 
        "Running RSEM on {wildcards.sample}"
    benchmark:
        "benchmarks/rsem/{sample}.rsem.benchmark"
    conda: "../envs/rsem_env.yml"
    params:
        sample_name = lambda wildcards: "analysis/rsem/{sample}/{sample}".format(sample=wildcards.sample),
        stranded = "--strand-specific" if config["stranded"] else "",
        paired_end = "--paired-end" if len(config["mate"]) == 2 else "",
        gz_support="--star-gzipped-read-file" if config["samples"][metadata.index[0]][0][-3:] == '.gz' else ""
    threads: 2
    shell:
        "rsem-calculate-expression -p {threads} {params.stranded} "
        "{params.paired_end} "
        "--estimate-rspd "
        "--bam --no-bam-output "
        "{input} {config[rsem_index]} "
        "{params.sample_name} > {log}"
        # "rsem-calculate-expression -p {threads} {params.stranded} "
  #       "{params.paired_end} "
  #       "--star {params.gz_support} "
  #       "--estimate-rspd --append-names "
  #       "{input} {config[rsem_index]} "
  #       "{params.sample_name} > {log}"

rule rsem_iso_matrix:
    input:
        rsem_iso_files = expand( "analysis/rsem/{sample}/{sample}.isoforms.results", sample=config["samples"] ),
        metasheet = config['metasheet']
    output:
        rsem_iso_matrix = "analysis/rsem/rsem_tpm_iso_matrix.csv"
    message: 
        "Running RSEM matrix generation rule for isoforms"
    benchmark:
        "benchmarks/rsem/rsem_iso_matrix.benchmark"
    params:
        args = lambda wildcards, input: merge_sep_inputs({input.rsem_iso_files})
    conda: "../envs/stat_perl_r.yml"
    shell:
        "perl src/raw_count_fpkm_tpm_matrix.pl --column 5 --metasheet {input.metasheet} --header -f {params.args} 1>{output.rsem_iso_matrix}"


rule rsem_gene_matrix:
    input:
        rsem_gene_files = expand( "analysis/rsem/{sample}/{sample}.genes.results", sample=config["samples"] ),
        metasheet = config["metasheet"]
    output:
        rsem_gene_matrix = "analysis/rsem/rsem_tpm_gene_matrix.csv"
    message: 
        "Running RSEM matrix generation rule for genes"
    benchmark:
        "benchmarks/rsem/rsem_gene_matrix.benchmark"
    params:
        args = lambda wildcards, input: merge_sep_inputs({input.rsem_gene_files})
    conda: "../envs/stat_perl_r.yml"
    shell:
        "perl src/raw_count_fpkm_tpm_matrix.pl --column 5 --metasheet {input.metasheet} --header -f {params.args} 1>{output.rsem_gene_matrix}"

rule salmon_quantification:
    input:
        "analysis/star/{sample}/{sample}.transcriptome.bam"
    output:
        "analysis/salmon/{sample}/{sample}.quant.sf"
    log:
        "analysis/salmon/{sample}/{sample}.transcriptome.bam.log"    
    params:
        index=config["salmon_index"],
        output_path="analysis/salmon/{sample}/",
    threads: _salmon_threads
    log: "logs/salmon/{sample}.salmon_quant.log"
    #conda: "../envs/salmon_env.yml"  ##no need to use salmon in conda as salmon is installed in the common environment
    message: "salmon: from bam to sf "
    benchmark:
        "benchmarks/salmon/{sample}.salmon.benchmark"   
    shell:
        "salmon quant -t {params.index} -l A -a {input} -o {params.output_path} "
        "-p {threads} "
        " && mv {params.output_path}/quant.sf {output}"

rule salmon_matrix:
    input:
        salmon_tpm_files = expand("analysis/salmon/{sample}/{sample}.quant.sf", sample = config['samples']),
        metasheet = config["metasheet"]
    output:
        "analysis/salmon/salmon_tpm_matrix.csv"
    benchmark:
        "benchmarks/salmon/salmon_gene_matrix.benchmark"
    params:
        args = lambda wildcards, input: merge_sep_inputs({input.salmon_tpm_files})
    #conda: "../envs/salmon_env.yml" ##no need to use salmon in conda as salmon is installed in the common environment
    message: "Merge Salmon gene quantification together for all samples "
    shell:
        "perl src/raw_count_fpkm_tpm_matrix.pl --metasheet {input.metasheet} -s --header -f {params.args} 1>{output}"



