#!/usr/bin/env python

#-------------------------------
# @author: Jin Wang; @ref: VIPER
# @email: jwang0611@gmail.com

def neoantigen_runsHelper(wildcards, iindex):
    """Given a snakemake wildcards, an iindex - 0 for Normal, 1 for Tumor,
    returns the sample name of Normal (if iindex=0) else sample name of Tmr"""
    tmp = []
    r = config['runs'][wildcards.run]
    #print(r)

    #check that we have a valid pair
    if len(r) >=2:
        sample_name = r[iindex]
        tmp.append(sample_name) 
    else:
        #NOTE: I can't figure out a proper kill command so I'll do this
        tmp=["ERROR! BAD pairing for run--requires at least two samples: %s" % (wildcards.run)]
    #print(tmp)
    return tmp

def neoantigen_getNormal(wildcards):
    return neoantigen_runsHelper(wildcards, 1)

def neoantigen_getTumor(wildcards):
    return neoantigen_runsHelper(wildcards, 0)

def neoantigen_targets(wildcards):
    """Generates the targets for this module"""
    ls = []
    for run in config['runs']:
        if len((config["runs"][run])) >= 2:
            tumor = config['runs'][run][0]
            ls.append("analysis/pvacseq/%s/%s.varscan.somatic.base.snp.Somatic.hc.filter.vep.vcf" % (run, run))
            ls.append("analysis/pvacseq/%s/%s.varscan.somatic.base.snp.Somatic.hc.filter.vep.gx.vcf" % (run, run))
            ls.append("analysis/pvacseq/%s/MHC_Class_I/%s.filtered.condensed.ranked.tsv" % (run,run))
            ls.append("analysis/pvacseq/%s/MHC_Class_I/%s.filtered.tsv" % (run, run))
            ls.append("analysis/pvacseq/%s/MHC_Class_I/%s.all_epitopes.tsv" % (run, run))
            ls.append("analysis/pvacseq/%s/MHC_Class_I/%s.tsv" % (run, run))
            ls.append("analysis/pvacseq/%s/%s.filtered.condensed.ranked.addSample.tsv" % (run, run))
    ls.append("files/pvacseq/Merged.filtered.condensed.ranked.addSample.tsv")
    ls.append("images/pvacseq/Patient_count_epitopes_plot.png")
    ls.append("images/pvacseq/epitopes_affinity_plot.png")
    ls.append("images/pvacseq/HLA_epitopes_fraction_plot.png")
    return ls

def getPvacseqOut(wildcards):
    """returns tuple (run, tumor sample name)"""
    run = wildcards.run
    tumor = config['runs'][run][0]
    return(run,tumor)

def getTumorHLA(wildcards):
    """get the optitype results file for the tumor sample"""
    run = wildcards.run
    tumor = config['runs'][run][0]
    ls = "analysis/optitype/%s/%s_result.tsv" % (tumor, tumor)
    #print(ls)
    return ls

def parseOptitype(optitype_out_file):
    """Given an optitypes '_results.tsv' file; parses the HLA A, B, C
    and returns these as a comma-separated string (for pvacseq) input

    NOTE: cureently the optitype results.tsv looks somthing like this:
    	  A1	    A2	B1	 B2	     C1	   C2	    Reads	Objective
    0	  	    		 	     C*06:04	    C*06:04	4.0	3.99
    **So were' going to parse cols 1-6 and return that"""
    #CATCH when the HLA does not exist yet
    #print(optitype_out_file)
    if not os.path.exists(optitype_out_file):
        print("WES ERROR: %s is not found!" % optitype_out_file)
        return ""

    f = open(optitype_out_file)
    hdr = f.readline().strip().split("\t") #ignore for now
    tmp = f.readline().strip().split("\t")[1:7] #want first 6 cols
        
    hla = ",".join(["HLA-%s" % x for x in tmp if x]) #ignore empty strings
    #print(hla)
    return hla

def pvacseq_extract(wildcards):
    ls=[]
    for run in config["runs"]: 
        if len((config["runs"][run])) >= 2:
            ls.append("analysis/pvacseq/%s/%s.filtered.condensed.ranked.addSample.tsv" % (run, run))
    return ls

rule neoantigen_all:
    input:
        neoantigen_targets

rule neoantigen_vep_annotate:
    input:
        "analysis/varscan/pair/{run}/{run}.varscan.somatic.base.snp.Somatic.hc.filter.vcf"
    output:
        "analysis/pvacseq/{run}/{run}.varscan.somatic.base.snp.Somatic.hc.filter.vep.vcf"
    params:
        index=config['fasta_path'],
        vep_data=config['vep_data'],
        vep_plugins=config['vep_plugins'],
        cache_version = config['cache_version']
        #normal = lambda wildcards: config['runs'][wildcards.run][0],
        #tumor = lambda wildcards: config['runs'][wildcards.run][1],
    log:
        "logs/pvacseq/{run}.neoantigen_vep_annotate.log"
    benchmark:
        "benchmarks/pvacseq/{run}.neoantigen_vep_annotate.benchmark"
    conda: "../envs/vep_env.yml"
    shell:
        "vep --input_file {input} --output_file {output} --format vcf --vcf --symbol --terms SO --tsl --hgvs --fasta {params.index} --offline --cache --dir_cache {params.vep_data} --cache_version {params.cache_version} --plugin Downstream --plugin Wildtype --dir_plugins {params.vep_plugins} --pick"


rule neoantigen_annotate_expression:
    input:
        vcf = "analysis/pvacseq/{run}/{run}.varscan.somatic.base.snp.Somatic.hc.filter.vep.vcf",
        expression = "analysis/combat/tpm_convertID.batch"
    output:
        "analysis/pvacseq/{run}/{run}.varscan.somatic.base.snp.Somatic.hc.filter.vep.gx.vcf"
    params:
        sample_name = lambda wildcards: config['runs'][wildcards.run][1],
        tmp = "analysis/pvacseq/{run}/{run}.tmp"
    benchmark:
        "benchmarks/pvacseq/{run}.neoantigen_vep_annotate.benchmark"
    # conda: "../envs/pvacseq_env.yml"
    shell:
        """tr ',' '\t' < {input.expression} | awk '{{if($1 != "gene_id") gsub(/\.[0-9]+/,"",$1)}}1' OFS='\t' > {params.tmp} """
        """ && {config[pvacseq_path]}/vcf-expression-annotator {input.vcf} {params.tmp} custom gene --id-column gene_id --expression-column {params.sample_name} -s TUMOR"""
        """ && rm {params.tmp}"""

rule neoantigen_pvacseq:
    """NOTE: neoantigen's pvacseq is not available on CONDA
    MUST either be installed in base system/docker container"""
    input:
        vcf="analysis/pvacseq/{run}/{run}.varscan.somatic.base.snp.Somatic.hc.filter.vep.gx.vcf",
        hla=getTumorHLA
    output:
        main = "analysis/pvacseq/{run}/MHC_Class_I/TUMOR.filtered.condensed.ranked.tsv",
        #OTHERS:
        filtered = "analysis/pvacseq/{run}/MHC_Class_I/TUMOR.filtered.tsv",
        all_epitopes = "analysis/pvacseq/{run}/MHC_Class_I/TUMOR.all_epitopes.tsv",
        tsv = "analysis/pvacseq/{run}/MHC_Class_I/TUMOR.tsv"
        # addSample = "analysis/pvacseq/{run}/TUMOR.filtered.condensed.ranked.addSample.tsv"
    params:
        normal = "NORMAL",#lambda wildcards: config['runs'][wildcards.run][0],
        tumor = "TUMOR",#lambda wildcards: config['runs'][wildcards.run][1],
        iedb = config['neoantigen_iedb_mhcI'],
        HLA = lambda wildcards,input: parseOptitype(input.hla),
        callers=config['neoantigen_callers'] if config['neoantigen_callers'] else "MHCflurry NetMHCcons MHCnuggetsII",
        epitope_lengths=config['neoantigen_epitope_lengths'] if config['neoantigen_epitope_lengths'] else "8,9,10,11",
        output_dir = "analysis/pvacseq/{run}"
    # threads: _neoantigen_threads
    # group: "neoantigen"
    log: 
        "logs/pvacseq/{run}.neoantigen_pvacseq.log"
    benchmark:
        "benchmarks/pvacseq/{run}.neoantigen_pvacseq.benchmark"
    # conda: "../envs/pvacseq_env.yml"
    shell:
        """{config[pvacseq_path]}/pvacseq run {input.vcf} {params.tumor} {params.HLA} {params.callers} {params.output_dir} -e {params.epitope_lengths} -t {threads} --normal-sample-name {params.normal} --iedb-install-directory {params.iedb} 2> {log} || true """
        """ && touch {output.main} """   ### to avoid there is no output from this run for some samples
        """ && touch {output.filtered} """
        """ && touch {output.all_epitopes} """
        """ && touch {output.tsv} """

       
rule pvacseq_merge:
    input:
        main="analysis/pvacseq/{run}/MHC_Class_I/TUMOR.filtered.condensed.ranked.tsv",
        filtered = "analysis/pvacseq/{run}/MHC_Class_I/TUMOR.filtered.tsv",
        all_epitopes = "analysis/pvacseq/{run}/MHC_Class_I/TUMOR.all_epitopes.tsv",
        tsv = "analysis/pvacseq/{run}/MHC_Class_I/TUMOR.tsv"
    output:
        main="analysis/pvacseq/{run}/MHC_Class_I/{run}.filtered.condensed.ranked.tsv",
        filtered = "analysis/pvacseq/{run}/MHC_Class_I/{run}.filtered.tsv",
        all_epitopes = "analysis/pvacseq/{run}/MHC_Class_I/{run}.all_epitopes.tsv",
        tsv = "analysis/pvacseq/{run}/MHC_Class_I/{run}.tsv",
        addSample = "analysis/pvacseq/{run}/{run}.filtered.condensed.ranked.addSample.tsv"
    shell:
        """mv {input.all_epitopes} {output.all_epitopes} && """
        """mv {input.main} {output.main} && """
        """mv {input.filtered} {output.filtered} && """
        """mv {input.tsv} {output.tsv} && """
        """awk '{{print ARGV[1]}}' {output.main} | awk -F '/' '{{print $3}}' | paste - {output.main}| awk -F '\t' 'NR==1{{$1="Sample"}}1' OFS='\t' > {output.addSample}"""

rule pvacseq_plot:
    input:
        pvacseq_extract
    output:
        merged_filter = "files/pvacseq/Merged.filtered.condensed.ranked.addSample.tsv",
        pat_epitopes = "images/pvacseq/Patient_count_epitopes_plot.png",
        epitope_affinity = "images/pvacseq/epitopes_affinity_plot.png",
        hla_epitope = "images/pvacseq/HLA_epitopes_fraction_plot.png"
    log:
        "logs/pvacseq/immune_pvacseq_plot.log"
    message:
        "Processing pvacseq result"
    benchmark:
        "benchmarks/pvacseq/pvacseq_plot.benchmark"
    params:
        outpath = "images/pvacseq/"
    conda: "../envs/stat_perl_r.yml"
    shell:
        """cat {input} | sed '1 !{{/Sample/d;}}' > {output.merged_filter} """  
        """ && Rscript src/pvacseq_plot.R --input {output.merged_filter} --outdir {params.outpath}"""     

        


  
